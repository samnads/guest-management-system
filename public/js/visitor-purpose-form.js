$().ready(function () {
    /***************************************************************************************** */
    $('#visitor_image,#visitor_image_upload').on("change", function (e) {
        var preview = document.getElementById('visitor_image_preview');
        preview.src = URL.createObjectURL(e.target.files[0]);
        var field_name = $(this).attr('name');
        if (field_name == "visitor_image_upload") {
            // because we use 2 upload functionality(direct camera and file select)
            $('#guest_add_form input[name="visitor_image"]')[0].files = $(
                '#guest_add_form input[name="visitor_image_upload"]')[0].files;
        }
    });
    /***************************************************************************************** */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#guest_add_form').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
    guest_add_form_validator = $('#guest_add_form').validate({
        focusInvalid: false,
        ignore: [],
        rules: rules,
        messages: rules_messages,
        errorPlacement: function (error, element) {
            console.log(error);
            if (element.attr("name") == "visitor_image") {
                Swal.fire({
                    title: 'Error !',
                    text: "Please upload a photo !",
                    icon: 'error',
                    confirmButtonText: 'OK',
                    allowOutsideClick: false
                });
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('#guest_add_form input[type="submit"]').val("Please wait...").attr("disabled",
                true);
            $.ajax({
                type: 'POST',
                url: _base_url + "guest/visitor/save",
                data: new FormData($('#guest_add_form')[0]),
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status == true) {
                        $('#guest_add_form input[type="submit"]').val("Done !")
                            .attr("disabled", true);
                        Swal.fire({
                            allowOutsideClick: false,
                            title: 'Done !',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        })
                            .then((value) => {
                                window.location.href = _base_url + "guest/lobby/" + building_id;
                            });
                    } else {
                        $('#guest_add_form input[type="submit"]').val(
                            "Submit Details").attr("disabled", false);
                        Swal.fire({
                            title: 'Error !',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        })
                    }
                },
                error: function (data) {
                    $('#guest_add_form input[type="submit"]').val("Submit Details")
                        .attr("disabled", false);
                    Swal.fire({
                        title: 'Error !',
                        text: data.statusText,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                },
            });
        }
    });
});