$(document).ready(function () {
    //-------------- Datatable ------------------------
    $('#flat-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: listFlatRoute,
            type: 'GET',
        },
        columns: [
            { data: 'flat_id', name: 'flat_id' },
            { data: 'flat_num', name: 'flat_num' },
            { data: 'building.building_name', name: 'building.building_name' },
            { data: 'username', name: 'username' },
            { data: 'password', name: 'password' },
            { data: 'action', name: 'action' },
        ],
        order: [[0, 'desc']],
        createdRow: function (row, data, dataIndex) {
            $(row).attr('id', 'row-' + data.flat_id); // Set custom ID to the <tr> element
            $(row).attr('data-building-id', + data.building_id);
        }
    });
    //--------------------------------------------------
    var flatIdToDelete;
    // Get the CSRF token from the meta tag
    var csrfToken = $('meta[name="csrf-token"]').attr('content');

    // Handle "Delete" button click
    $('#flat-table').on('click', '.delete-btn', function () {
        flatIdToDelete = $(this).data('id');
        $('#deleteModal').modal('show');
    });

    // Handle "Confirm Delete" button click
    $('#confirmDelete').on('click', function () {
        var url = deleteFlatRoute.replace(':flatId', flatIdToDelete);

        // You can add an AJAX call here to perform the delete action
        $.ajax({
            url: url,
            type: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the headers
            },
            success: function (response) {
                console.log('Deleting Flat ID: ' + flatIdToDelete);
                // If deletion is successful, remove the row from DataTable
                // Reload the DataTable
                $('#flat-table').DataTable().ajax.reload(null, false);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.error('Error deleting Flat:', errorThrown);
            }
        });
        // Close the modal
        $('#deleteModal').modal('hide');
    });

    //--------------------------------------------------
    $("#add-flat-btn").click(function(){
        $("#add-flat-popup-wrapper").show(500);
    });

    //---------------------------------------------------
    // Handle "ADD" button click
    $('#save-flat-btn').on('click', function () {
        var flatName = $('#flat_name').val();
        var buildingId = $('#building_select_form').val();
        var flatUsername = $('#flat_username').val();
        var flatPassword = $('#flat_password').val();

        // Send AJAX request to add building
        $.ajax({
            url: addFlatRoute,
            type: 'POST',
            data: {
                flat_num: flatName,
                building_id: buildingId,
                username: flatUsername,
                password: flatPassword
            },
            headers: {
                'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the headers
            },
            success: function (response) {
                // Reload the DataTable
                $('#flat-table').DataTable().ajax.reload(null, false);
                // Handle success
                console.log('Flat added successfully:', response);
                $("#add-flat-popup-wrapper").hide(500);
            },
            error: function (xhr, textStatus, errorThrown) {
                if (xhr.status === 422) {
                    // Handle validation errors
                    var errors = xhr.responseJSON.errors;
                    console.error('Validation errors:', errors);
                    displayTranslatedValidationErrors(errors);
                } else {
                    console.error('Error adding flat:', xhr.responseText);
                }
            }
        });
    });
    function displayTranslatedValidationErrors(errors) {
        // Clear previous error messages
        $('.text-danger').text('');

        // Display translated validation errors next to corresponding fields
        if (errors.flat_num) {
            $('#flat_name_error').text(trans(errors.flat_num[0]));
        }
        if (errors.building_id) {
            $('#building_select_form_error').text(trans(errors.building_id[0]));
        }
        if (errors.username) {
            $('#flat_username_error').text(trans(errors.username[0]));
        }
        if (errors.password) {
            $('#flat_password_error').text(trans(errors.password[0]));
        }
    }
    var translations = {
        'validation.required': 'This field is required.',
        'validation.max.string': 'Max length exceeded.'
    };
    function trans(key) {
        return translations[key] || key;
    }
    //-------------------------------------------------------------------------------
    var flatIdToEdit;
    var BuildingId =
    $('#flat-table').on('click', '.edit-btn', function () {
        // Get the row data associated with the clicked "Edit" button
        flatIdToEdit = $(this).data('id');
        var BuildingId = $('#row-2526').data('building-id');
        var row = $(this).closest('tr');

        // Retrieve text content of specific cells by cell number
        var FlatName = row.find('td:eq(1)').text(); // Cell 1
        //var BuildingName = row.find('td:eq(2)').text(); // Cell 2
        var UserName = row.find('td:eq(3)').text();
        var Password = row.find('td:eq(4)').text();


        // Update the data-id attribute of the "Edit" button
        $('#edit-Flat-btn').data('id', flatIdToEdit);
        $('#edit-Flat-btn').data('buiding-id', BuildingId);

        // Populate input fields with the retrieved data
        $('#flat_name_edit').val(FlatName);
        $('#building_select_form_edit').val(BuildingId).trigger('change');
        $('#flat_username_edit').val(UserName);
        $('#flat_password_edit').val(Password);

        $("#edit-flat-popup-wrapper").show(500);
    });

    $('#edit-flat-btn').on('click', function () {
        var url = editFlatRoute.replace(':flatId', flatIdToEdit);


        var flatName = $('#flat_name_edit').val();
        var buildingId = $('#building_select_form_edit').val();
        var flatUsername = $('#flat_username_edit').val();
        var flatPassword = $('#flat_password_edit').val();

        // Perform the AJAX update
        $.ajax({
            url: url,
            method: 'PUT', // Use 'PUT' for update
            headers: {
                'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the headers
            },
            data: {
                flat_num: flatName,
                building_id: buildingId,
                username: flatUsername,
                password: flatPassword
            },
            success: function (response) {
                // Handle success response, e.g., close modal, update table, etc.
                console.log('Update successful:', response);
                // Close the modal
                $("#edit-flat-popup-wrapper").hide(500);

                // You might want to reload the DataTable to reflect the updated data
                $('#flat-table').DataTable().ajax.reload();
            },
            error: function (xhr, status, error) {
                // Handle error response, e.g., show error message
                console.error('Update error:', error);
            }
        });
    });
});
