$(document).ready(function () {
    // Function to generate a random color
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    //====================================  =======================
    var barChartData = {
        labels: timerange,
        datasets: [{
            label: 'User',
            backgroundColor: "#1fb8cb",
            data: vistors
        }]
    };

    var barChartData2 = {
        labels: flats,
        datasets: [{
            label: 'User',
            backgroundColor: "#3958b2",
            data: visitorByFlat
        }]
    };

    // Generate dynamic colors for each purpose
    var dynamicColors1 = [];
    for (var i = 0; i < Object.keys(visitorByPurpose).length; i++) {
        var color = getRandomColor();
        dynamicColors1.push(color);
    }

    var pieChartData1 = {
        labels: purposes,
        datasets: [{
            label: 'Purpose',
            backgroundColor: dynamicColors1,
            data: visitorByPurpose
        }]
    };

    var dynamicColors2 = [];
    for (var i = 0; i < Object.keys(visitorByPurposeLastMonth).length; i++) {
        var color = getRandomColor();
        dynamicColors2.push(color);
    }
    var doughnutChartData2 = {
        labels: purposesLastMonth,
        datasets: [{
            label: 'Purpose',
            backgroundColor: dynamicColors2,
            data: visitorByPurposeLastMonth
        }]
    };
    window.onload = function() {
        var ctx = document.getElementById("last24hours").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: '#c1c1c1',
                        borderSkipped: 'bottom'
                    }
                },
                legend: {
                    display: false // Hide the legend
                },
                responsive: true,
                title: {
                    display: false,
                    text: ''
                }
            }
        });

    //==================================== Flat visitor count =======================
        var ctx2 = document.getElementById("flatVisitorCount").getContext("2d");
        window.myBar = new Chart(ctx2, {
            type: 'bar',
            data: barChartData2,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: '#c1c1c1',
                        borderSkipped: 'bottom'
                    }
                },
                legend: {
                    display: false // Hide the legend
                },
                responsive: true,
                title: {
                    display: false,
                    text: ''
                }
            }
        });

    //==================================== Purpose visitor count =======================
    var ctx3 = document.getElementById("byPurpose").getContext("2d");
    window.myBar = new Chart(ctx3, {
        type: 'pie',
        data: pieChartData1,
        options: {
            elements: {
                rectangle: {
                    borderWidth: 2,
                    borderColor: '#c1c1c1',
                    borderSkipped: 'bottom'
                }
            },
            responsive: true,
            title: {
                display: false,
                text: ''
            }
        }
    });

     //==================================== Last month visitor count =======================
     var ctx4 = document.getElementById("lastMonth").getContext("2d");
     window.myBar = new Chart(ctx4, {
         type: 'doughnut',
         data: doughnutChartData2,
         options: {
             elements: {
                 rectangle: {
                     borderWidth: 2,
                     borderColor: '#c1c1c1',
                     borderSkipped: 'bottom'
                 }
             },
             responsive: true,
             title: {
                 display: false,
                 text: ''
             }
         }
     });

     //=================================================================

    //------------------------------------------------------------------
    };
});
