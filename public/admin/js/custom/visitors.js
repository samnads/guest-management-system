$(function () {
    // Get the CSRF token from the meta tag
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    //==================================================================================
    var visitorTable = $("#visitorTable").DataTable({
        processing: true,
        serverSide: true,
        dom: "Bfrtip",
        buttons: ["copyHtml5", "excelHtml5", "csvHtml5", "pdfHtml5"],
        ajax: {
            url: vistorRoute,
            type: "GET",
            data: function (d) {
                // Include any additional data here (if needed)
            },
        },
        columns: [
            { data: "user_id", name: "user_id", className: "Cell sl-number" },
            { data: "name", name: "name", className: "Cell sl-number" },
            { data: "mobile", name: "mobile", className: "Cell sl-number" },
            { data: "idnumber", name: "idnumber", className: "Cell sl-number" },
            {
                data: "id_expiry_date",
                name: "id_expiry_date",
                className: "Cell sl-number",
            },
            {
                data: "company_name",
                name: "company_name",
                className: "Cell sl-number",
            },
            {
                data: "building.building_name",
                name: "building.building_name",
                className: "Cell sl-number",
            },
            {
                data: "flat.flat_num",
                name: "flat.flat_num",
                className: "Cell sl-number",
            },
            { data: "purpose", name: "purpose", className: "Cell sl-number" },
            {
                data: "create_date",
                name: "create_date",
                className: "Cell sl-number",
            },
            {
                data: "accepted_status",
                name: "accepted_status",
                className: "Cell sl-number",
                render: function (data, type, row) {
                    if (data === 0) {
                        return "Pending";
                    } else if (data === 1) {
                        return "Accepted";
                    } else if (data === 2) {
                        return "Rejected";
                    }
                    return ""; // Return an empty string for other cases
                },
            },
            { data: "action", name: "action", className: "Cell sl-number" },
        ],
    });
    //==========================================================================
    $(document).on("click", ".accept-btn", function () {
        var userId = $(this).data("id");
        var url = acceptRoute.replace(":id", userId);
        $.ajax({
            url: url,
            type: "PATCH",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": csrfToken, // Include the CSRF token in the headers
            },
            success: function (response) {
                if (response.success) {
                    // Reload the DataTable
                    $("#visitorTable").DataTable().ajax.reload(null, false);
                    // Update the table or perform other actions
                    alert("Visitor accepted successfully.");
                } else {
                    alert("An error occurred.");
                }
            },
            error: function (xhr, status, error) {
                console.error(error);
                alert("An error occurred.");
            },
        });
    });
    //==========================================================================
    $(document).on("click", ".reject-btn", function () {
        var userId = $(this).data("id");
        var url = rejectRoute.replace(":id", userId);
        $.ajax({
            url: url,
            type: "PATCH",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": csrfToken, // Include the CSRF token in the headers
            },
            success: function (response) {
                if (response.success) {
                    // Reload the DataTable
                    $("#visitorTable").DataTable().ajax.reload(null, false);
                    // Update the table or perform other actions
                    alert("Visitor rejected successfully.");
                } else {
                    alert("An error occurred.");
                }
            },
            error: function (xhr, status, error) {
                console.error(error);
                alert("An error occurred.");
            },
        });
    });
    //==========================================================================
    // Initialize the first datepicker
    $("#from-date").datepicker({
        dateFormat: "yy-mm-dd", // Set the desired date format
        autoclose: true,
    });
    //==========================================================================
    // Initialize the second datepicker with a different configuration
    $("#to-date").datepicker({
        dateFormat: "yy-mm-dd", // Set the desired date format
        autoclose: true,
    });
    //==========================================================================
    $("#search_vistors").on("click", function () {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();
        var selectedPurpose = $("#purpose").val();

        // Update the DataTable's AJAX parameters
        visitorTable.ajax
            .url(
                vistorRoute +
                    "?from_date=" +
                    fromDate +
                    "&to_date=" +
                    toDate +
                    "&purpose=" +
                    selectedPurpose
            )
            .load();
    });
    //==========================================================================
    $("#exportButton").on("click", function () {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();
        var selectedPurpose = $("#purpose").val();
        var exportType = "Excel";
        var searchInput = visitorTable.search();
        var filters = {
            custom: {
                from_date: fromDate,
                to_date: toDate,
                purpose: selectedPurpose,
            },
            search: searchInput,
        };
        $.ajax({
            url: exportRoute, // Replace with your export route
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrfToken, // Include the CSRF token in the headers
            },
            data: {
                filters: filters,
                exportType: exportType,
            },
            success: function (response) {
                if (response.file_url) {
                    // Create a link and simulate clicking to trigger download
                    var link = document.createElement("a");
                    link.href = response.file_url;
                    var filename = response.file_url.substring(
                        response.file_url.lastIndexOf("/") + 1
                    );
                    link.download = filename; // Set the desired filename for download
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                } else if (response.error) {
                    // Call the showErrorToast function with the error message
                    showErrorToast(response.error);
                }
            },
            error: function () {
                // Handle errors
            },
        });
    });

    //==========================================================================
    function showErrorToast(message) {
        // Create the error message div
        var errorDiv = $("<div>", {
            class: "alert alert-danger alert-dismissible fade show",
            role: "alert",
        });

        // Create the error message text
        var errorMessage = document.createTextNode(message);
        errorDiv.append(errorMessage);

        // Create the close button
        var closeButton = $("<button>", {
            type: "button",
            class: "close",
            "data-dismiss": "alert",
            "aria-label": "Close",
        });

        // Create the close icon
        var closeIcon = $("<span>", {
            "aria-hidden": "true",
            html: "&times;",
        });

        closeButton.append(closeIcon);
        errorDiv.append(closeButton);

        // Get the element with class .dashboard
        var dashboardElement = $(".dashboard-tmb-wrapper");

        // Append the error message div after the .dashboard element
        dashboardElement.after(errorDiv);
    }
});
