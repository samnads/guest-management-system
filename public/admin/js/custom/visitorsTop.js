$(function () {
    // Get the CSRF token from the meta tag
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    //==================================================================================
    var visitorTopTable = $("#visitorTopTable").DataTable({
        processing: true,
        serverSide: true,
        dom: "Bfrtip",
        buttons: ["copyHtml5", "excelHtml5", "csvHtml5", "pdfHtml5"],
        ajax: {
            url: vistorRouteTop,
            type: "GET",
            data: function (d) {
                // Include any additional data here (if needed)
            },
        },
        columns: [
            { data: "user_id", name: "user_id", className: "Cell sl-number" },
            { data: "name", name: "name", className: "Cell sl-number" },
            { data: "Visits", name: "Visits", className: "Cell sl-number" },
            { data: "mobile", name: "mobile", className: "Cell sl-number" },
            { data: "building.building_name",name: "building.building_name",className: "Cell sl-number"},
            { data: "flat.flat_num",name: "flat.flat_num",className: "Cell sl-number"},
            { data: "purpose", name: "purpose", className: "Cell sl-number" },
            { data: "accepted_status",name: "accepted_status",className: "Cell sl-number",
                render: function (data, type, row) {
                    if (data === 0) {
                        return "Pending";
                    } else if (data === 1) {
                        return "Accepted";
                    } else if (data === 2) {
                        return "Rejected";
                    }
                    return ""; // Return an empty string for other cases
                },
            },
            { data: "Entry_Date",name: "Entry_Date",className: "Cell sl-number"}
        ],
        order: [[2, 'desc']],
    });
    //==========================================================================
    // Initialize the first datepicker
    $("#from-date").datepicker({
        dateFormat: "yy-mm-dd", // Set the desired date format
        autoclose: true,
    });
    //==========================================================================
    // Initialize the second datepicker with a different configuration
    $("#to-date").datepicker({
        dateFormat: "yy-mm-dd", // Set the desired date format
        autoclose: true,
    });
    //==========================================================================
    $("#search_vistors").on("click", function () {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();
        var selectedPurpose = $("#purpose").val();

        // Update the DataTable's AJAX parameters
        visitorTopTable.ajax
            .url(
                vistorRouteTop +
                    "?from_date=" +
                    fromDate +
                    "&to_date=" +
                    toDate +
                    "&purpose=" +
                    selectedPurpose
            )
            .load();
    });
    //==========================================================================
    $("#exportButton").on("click", function () {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();
        var selectedPurpose = $("#purpose").val();
        var exportType = "Excel";
        var searchInput = visitorTopTable.search();
        var filters = {
            custom: {
                from_date: fromDate,
                to_date: toDate,
                purpose: selectedPurpose,
            },
            search: searchInput,
            listTop: true
        };
        $.ajax({
            url: exportRouteTop, // Replace with your export route
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrfToken, // Include the CSRF token in the headers
            },
            data: {
                filters: filters,
                exportType: exportType,
            },
            success: function (response) {
                if (response.file_url) {
                    // Create a link and simulate clicking to trigger download
                    var link = document.createElement("a");
                    link.href = response.file_url;
                    var filename = response.file_url.substring(
                        response.file_url.lastIndexOf("/") + 1
                    );
                    link.download = filename; // Set the desired filename for download
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                } else if (response.error) {
                    // Call the showErrorToast function with the error message
                    showErrorToast(response.error);
                }
            },
            error: function () {
                // Handle errors
            },
        });
    });

    //==========================================================================
    function showErrorToast(message) {
        // Create the error message div
        var errorDiv = $("<div>", {
            class: "alert alert-danger alert-dismissible fade show",
            role: "alert",
        });

        // Create the error message text
        var errorMessage = document.createTextNode(message);
        errorDiv.append(errorMessage);

        // Create the close button
        var closeButton = $("<button>", {
            type: "button",
            class: "close",
            "data-dismiss": "alert",
            "aria-label": "Close",
        });

        // Create the close icon
        var closeIcon = $("<span>", {
            "aria-hidden": "true",
            html: "&times;",
        });

        closeButton.append(closeIcon);
        errorDiv.append(closeButton);

        // Get the element with class .dashboard
        var dashboardElement = $(".dashboard-tmb-wrapper");

        // Append the error message div after the .dashboard element
        dashboardElement.after(errorDiv);
    }
});
