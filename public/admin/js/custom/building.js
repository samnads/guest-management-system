$(document).ready(function () {
    //-------------- Datatable ------------------------
    $('#building-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: listBuildingRoute,
            type: 'GET',
        },
        columns: [
            { data: 'building_id', name: 'building_id' },
            { data: 'building_name', name: 'building_name' },
            { data: 'code', name: 'code' },
            { data: 'action', name: 'action' },
        ],
        order: [[0, 'desc']],
        createdRow: function (row, data, dataIndex) {
            $(row).attr('id', 'row-' + data.building_id); // Set custom ID to the <tr> element
        }
    });
    //--------------------------------------------------
    var buildingIdToDelete;
    // Get the CSRF token from the meta tag
    var csrfToken = $('meta[name="csrf-token"]').attr('content');

    // Handle "Delete" button click
    $('#building-table').on('click', '.delete-btn', function () {
        buildingIdToDelete = $(this).data('id');
        $('#deleteModal').modal('show');
    });

    // Handle "Confirm Delete" button click
    $('#confirmDelete').on('click', function () {
        var url = deleteBuildingRoute.replace(':buildingId', buildingIdToDelete);

        // You can add an AJAX call here to perform the delete action
        $.ajax({
            url: url,
            type: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the headers
            },
            success: function (response) {
                console.log('Deleting Building ID: ' + buildingIdToDelete);
                // If deletion is successful, remove the row from DataTable
                // Reload the DataTable
                $('#building-table').DataTable().ajax.reload(null, false);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.error('Error deleting building:', errorThrown);
            }
        });
        // Close the modal
        $('#deleteModal').modal('hide');
    });

    //--------------------------------------------------
    $(".show-add-member-btn").click(function(){
        $("#add-member-popup-wrapper").show(500);
    });

    //---------------------------------------------------
    // Handle "ADD" button click
    $('#add-building-btn').on('click', function () {
        var buildingName = $('#building_name').val();
        var buildingCode = $('#building_code').val();

        // Send AJAX request to add building
        $.ajax({
            url: addBuildingRoute,
            type: 'POST',
            data: {
                building_name: buildingName,
                building_code: buildingCode
            },
            headers: {
                'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the headers
            },
            success: function (response) {
                // Reload the DataTable
                $('#building-table').DataTable().ajax.reload(null, false);
                // Handle success
                console.log('Building added successfully:', response);
                $("#add-member-popup-wrapper").hide(500);
            },
            error: function (xhr, textStatus, errorThrown) {
                if (xhr.status === 422) {
                    // Handle validation errors
                    var errors = xhr.responseJSON.errors;
                    console.error('Validation errors:', errors);
                    displayTranslatedValidationErrors(errors);
                } else {
                    console.error('Error adding building:', xhr.responseText);
                }
            }
        });
    });
    function displayTranslatedValidationErrors(errors) {
        // Clear previous error messages
        $('.text-danger').text('');

        // Display translated validation errors next to corresponding fields
        if (errors.building_name) {
            $('#building_name_error').text(trans(errors.building_name[0]));
        }
        if (errors.building_code) {
            $('#building_code_error').text(trans(errors.building_code[0]));
        }
    }
    var translations = {
        'validation.required': 'This field is required.',
        'validation.max.string': 'Max length exceeded.'
    };
    function trans(key) {
        return translations[key] || key;
    }
    //-------------------------------------------------------------------------------
    var buildingIdToEdit;
    $('#building-table').on('click', '.edit-btn', function () {
        // Get the row data associated with the clicked "Edit" button
        buildingIdToEdit = $(this).data('id');
        var row = $(this).closest('tr');

        // Retrieve text content of specific cells by cell number
        var buildingName = row.find('td:eq(1)').text(); // Cell 1
        var buildingCode = row.find('td:eq(2)').text(); // Cell 2

        // Update the data-id attribute of the "Edit" button
        $('#edit-building-btn').data('id', buildingIdToEdit);

        // Populate input fields with the retrieved data
        $('#building_name_edit').val(buildingName);
        $('#building_code_edit').val(buildingCode);
        $("#edit-member-popup-wrapper").show(500);
    });

    $('#edit-building-btn').on('click', function () {
        var buildingIdToEdit = $(this).data('id');
        var url = editBuildingRoute.replace(':buildingId', buildingIdToEdit);
        var buildingName = $('#building_name_edit').val();
        var buildingCode = $('#building_code_edit').val();

        // Perform the AJAX update
        $.ajax({
            url: url,
            method: 'PUT', // Use 'PUT' for update
            headers: {
                'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the headers
            },
            data: {
                building_name: buildingName,
                building_code: buildingCode
            },
            success: function (response) {
                // Handle success response, e.g., close modal, update table, etc.
                console.log('Update successful:', response);
                // Close the modal
                $("#edit-member-popup-wrapper").hide(500);

                // You might want to reload the DataTable to reflect the updated data
                $('#building-table').DataTable().ajax.reload();
            },
            error: function (xhr, status, error) {
                // Handle error response, e.g., show error message
                console.error('Update error:', error);
            }
        });
    });
});
