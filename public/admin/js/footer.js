$(function() {
    //==============================================
    $(".menu-icon").click(function(){
        $(".main-nav").toggle(500);
    });
    //==============================================
	$(".top-dropdown-set").click(function(){
        $(".top-dropdown").toggle(500);
    });
    //==============================================
	$(".top-search-icon").click(function(){
        console.log("DOM is fully loaded!");
        $(".top-search-main").toggle(500);
    });
    //==============================================
	$(".delete-action").click(function(){
	  $(this).closest(".Row").remove();
	  $(this).closest("tr").remove();
	});
    //==============================================
    $('.tooltip-ation-main').on('click', function() {
        alert(1);
		$('.tooltip-ation-main').removeClass('active');
		$(this).toggleClass('active');
    });
    //==============================================
	$(".active-action").click(function(){
		$(".active-action i").toggleClass("fa-toggle-on");
		$(".active-action i").toggleClass("fa-toggle-off");
    });
    //==============================================
	$(".close-btn").click(function(){
        $(".login-wrapper, .add-member-popup-wrapper, .common-popup-wrapper").hide(500);
    });
    //==============================================
    // Event delegation for the "tooltip-ation-main" class
    $('.Table').on('click', '.tooltip-ation-main', function() {
        alert(1);
        $('.tooltip-ation-main').removeClass('active');
        $(this).toggleClass('active');
    });
    //==============================================
    // Get the select element
    const buildingSelect = $('#buildingSelect');
    // Listen for the change event
    buildingSelect.on('change', function() {
        // Get the selected building's ID
        const selectedBuildingId = $(this).val();
        // Get the CSRF token from the meta tag
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        // Make an AJAX request to update the session variable
        $.ajax({
            url: buildingSessionSetRoute,
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the headers
            },
            data: { buildingId: selectedBuildingId },
            success: function(response) {
                // After the session is updated successfully, reload the page
                location.reload(); // This will refresh the current page
                // Handle the success response if needed
                console.log('Session variable updated successfully');
            },
            error: function(xhr, status, error) {
                // Handle the error if needed
                console.error('An error occurred while updating the session variable');
            }
        });
    });
});
