<?php

namespace App\DataTables;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Repositories\BuildingRepository as BuildingRepo;
use Exception;

class BuildingDataTable
{
    public function get(Request $request)
    {
        try {
            $buildingRepo = new BuildingRepo();
            $filters = $this->setFilters($request);
            $orderBy = $this->setOrder($request);
            $relations = $this->setRelations();
            $skip = $request->start;
            $take = $request->length;

            $buildings = $buildingRepo->list($filters, $skip, $take, $orderBy, $relations);
            $buildingCount = $buildingRepo->getListCount();

            $dataTable = Datatables::of($buildings);
            $dataTable->setTotalRecords($buildingCount)->skipPaging();

            return $dataTable->addColumn('action', function ($building) {
                return '<button class="btn btn-success btn-sm my-2 edit-btn m-3" data-id="' . $building->building_id . '">Edit</button>'
                     . '<button class="btn btn-danger btn-sm my-2 delete-btn m-3" data-id="' . $building->building_id . '">Delete</button>';
            })->make(true);

        } catch (Exception $ex) {
            Log::error($ex);
        }
    }

    private function setFilters($request)
    {
        $filters = [];
        $filters['search'] = $request->search['value'];
        return $filters;
    }

    private function setOrder($request)
    {
        $columns = $request->columns;
        $orderBy = [];
        if (empty($request->order)) {
            return $orderBy;
        }
        $orders = $request->order;
        foreach ($orders as $order) {
            $orderCol = $order['column'];
            $colName = $columns[$orderCol]['name'];
            array_push($orderBy, [$colName, $order['dir']]);
        }
        return $orderBy;
    }

    private function setRelations()
    {
        // Set any necessary relations for display values
        return $relations = [];
    }
}
