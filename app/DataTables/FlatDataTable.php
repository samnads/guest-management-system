<?php

namespace App\DataTables;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Repositories\FlatRepository as FlatRepo;
use Exception;

class FlatDataTable
{
    public function get(Request $request)
    {
        try {
            $flatRepo = new FlatRepo();
            $filters = $this->setFilters($request);
            $orderBy = $this->setOrder($request);
            $relations = $this->setRelations();
            $skip = $request->start;
            $take = $request->length;

            $flats = $flatRepo->list($filters, $skip, $take, $orderBy, $relations);
            $flatCount = $flatRepo->getListCount();

            $dataTable = Datatables::of($flats);
            $dataTable->setTotalRecords($flatCount)->skipPaging();

            return $dataTable->addColumn('action', function ($flat) {
                return '<button class="btn btn-success btn-sm my-2 edit-btn m-3" data-id="' . $flat->flat_id . '">Edit</button>'
                     . '<button class="btn btn-danger btn-sm my-2 delete-btn m-3" data-id="' . $flat->flat_id . '">Delete</button>';
            })->make(true);

        } catch (Exception $ex) {
            Log::error($ex);
        }
    }

    private function setFilters($request)
    {
        $filters = [];
        $filters['search'] = $request->search['value'];
        return $filters;
    }

    private function setOrder($request)
    {
        $columns = $request->columns;
        $orderBy = [];
        if (empty($request->order)) {
            return $orderBy;
        }
        $orders = $request->order;
        foreach ($orders as $order) {
            $orderCol = $order['column'];
            $colName = $columns[$orderCol]['name'];
            array_push($orderBy, [$colName, $order['dir']]);
        }
        return $orderBy;
    }

    private function setRelations()
    {
        // Set any necessary relations for display values
        return $relations = ['building'];
    }
}
