<?php
namespace App\DataTables;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Repositories\VistorRepository as VistorRepo;
use Exception;

class VistorDataTableTop
{
    /**=============================================
     * Function to get datatable data
    ================================================ */
    public function get(Request $request){
        try{
            $vistorsObj = new VistorRepo();
            $filters = $this->setFilters($request);
            $orderBy = $this->setOrder($request);
            $relations = $this->setRelations();
            $skip = $request->start;
            $take = $request->length;
            $filters['listTop'] = true;
            $vistors = $vistorsObj->list($filters,$skip,$take,$orderBy,$relations);
            $visitorCount = $vistorsObj->getListCount();
            $dataTable = Datatables::of($vistors);
            $dataTable->setTotalRecords($visitorCount)->skipPaging();
            return $dataTable->make(true);

        } catch (Exception $ex) {
            Log::error($ex);
        }
    }
    /**=============================================
     * Function to set Filter's if exist
     * @param type $request
    ================================================ */
    private function setFilters($request){
        $filters = [];
        if(isset($request->from_date)){
            $filters['custom']['from_date'] = $request->from_date;
        }

        if(isset($request->to_date)){
            $filters['custom']['to_date'] = $request->to_date;
        }

        if(isset($request->purpose)){
            $filters['custom']['purpose'] = $request->purpose;
        }

        $filters['search'] = $request->search['value'];
        return $filters;
    }
    /**=============================================
     * Function to set Order of meter reading list
     * @param type $request
     * @return array
      ================================================*/
    private function setOrder($request){
        $columns = $request->columns;
        $orderBy = [];
        if(empty($request->order)){
            return $orderBy;
        }
        $orders = $request->order;
        foreach($orders as $order){
            $orderCol = $order['column'];
            $colName = $columns[$orderCol]['name'];
            array_push($orderBy,[$colName,$order['dir']]);
        }
       return $orderBy;
    }
    /**==============================================
     * Function to set relations to get display values
     ================================================*/
    private function setRelations(){
        return $relations = ['purposeOfVisit','building','flat'];
    }
}
