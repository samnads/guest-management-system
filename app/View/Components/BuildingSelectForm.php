<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Buildings;

class BuildingSelectForm extends Component
{
    public $buildings;
    public $selectId;

    public function __construct($selectId)
    {
        $this->buildings = Buildings::where('status', 1)->get();
        $this->selectId = $selectId;
    }

    public function render()
    {
        return view('components.form-elements.building-select-form');
    }
}
