<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\PurposeOfVisit;

class PurposeListDropdown extends Component
{
    public $purposes;
    public $selectedPurpose;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($selectedPurpose = null)
    {
        $this->selectedPurpose = $selectedPurpose;
        $this->purposes = PurposeOfVisit::where('status', 1)->get();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.purpose-list-dropdown');
    }
}
