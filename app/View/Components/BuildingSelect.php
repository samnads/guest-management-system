<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Buildings;

class BuildingSelect extends Component
{
    public $buildings;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->buildings = Buildings::where('status', 1)->get();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.building-select');
    }
}
