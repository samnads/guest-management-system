<?php
namespace App\Services;

use Carbon\Carbon;
use App\Repositories\VistorRepository as VistorRepo;

class VisitorService
{
    protected $vistorRepo;

    public function __construct()
    {
        $this->vistorRepo = new VistorRepo();
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getVisitorsCount()
    {
        $counts = [
            'today' => $this->getTodayVisitorsCount(),
            'lastWeek' => $this->getLastWeekVisitorsCount(),
            'lastMonth' => $this->getLastMonthVisitorsCount(),
            'total' => $this->getTotalVisitorsCount(),
        ];

        return $counts;
    }
    /**====================================================
     * Function to
    ======================================================= */
    protected function getTodayVisitorsCount()
    {
        return $this->vistorRepo->getVisitorsCountByDate(Carbon::today());
    }
    /**====================================================
     * Function to
    ======================================================= */
    protected function getLastWeekVisitorsCount()
    {
        $lastWeekStartDate = Carbon::today()->subDays(7);
        return $this->vistorRepo->getVisitorsCountByDateRange($lastWeekStartDate, Carbon::today());
    }

    protected function getLastMonthVisitorsCount()
    {
        $lastMonthStartDate = Carbon::today()->subMonth();
        return $this->vistorRepo->getVisitorsCountByDateRange($lastMonthStartDate, Carbon::today());
    }
    /**====================================================
     * Function to
    ======================================================= */
    protected function getTotalVisitorsCount()
    {
        return $this->vistorRepo->getTotalVisitorsCount();
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getLast24HoursVisitorsCountByHour()
    {
        $hourlyCounts = $this->vistorRepo->getVisitorsCountByHourRange();

        $hourlyData = [];
        for ($hour = 0; $hour < 24; $hour++) {
            $hourLabel = sprintf('%02d:00 - %02d:00', $hour, ($hour + 1) % 24);
            $count = 0;

            foreach ($hourlyCounts as $hourlyCount) {
                if ($hourlyCount->hour === $hour) {
                    $count = $hourlyCount->count;
                    break;
                }
            }

            $hourlyData[$hourLabel] = $count;
        }

        return $hourlyData;
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getVisitorCountByFlat()
    {
        return $this->vistorRepo->getVisitorCountByFlat();
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getVisitorCountByPurpose()
    {
        return $this->vistorRepo->getVisitorCountByPurpose();
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getVisitorCountByPurposeLastMonth()
    {
        return $this->vistorRepo->getVisitorCountByPurposeLastMonth();
    }
}
