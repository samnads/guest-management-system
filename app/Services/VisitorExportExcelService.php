<?php
// app/Services/VisitorExportService.php
namespace App\Services;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

use App\Repositories\VistorRepository as VistorRepo;

class VisitorExportExcelService implements FromCollection
{
    protected $vistorRepo;
    protected $maxRowCount = 1000;
    protected $currentRowCount;

    protected $filters = [];
    protected $orderBy;
    protected $skip = 0;
    protected $take = 0;
    protected $relations = [];
    protected $fields = [];

    /**====================================================
     * Function constructor
    ======================================================= */
    public function __construct()
    {
        $this->vistorRepo = new VistorRepo();
    }

    /**====================================================
     * Function to get data for excel
    ======================================================= */
    public function collection()
    {
        $vistors = $this->vistorRepo->list($this->filters, $this->skip, $this->take, $this->orderBy, $this->relations, $this->fields);
        $onevistor = $vistors->first();
        // Convert the model properties to an array
        $columnTitles = array_keys($onevistor->toArray());

        // Modify column titles to title case with spaces
        $columnTitles = array_map(function ($title) {
            return ucwords(str_replace('_', ' ', $title));
        }, $columnTitles);
        // Prepend column titles to the data collection
        $vistors->prepend($columnTitles);
        return $vistors;
    }

    /**====================================================
     * Function to export vistor data as excel
    ======================================================= */
    public function export($filters)
    {
        $this->filters = $filters;
        // Check if total records exceed the maximum row size
        $this->collection();
        $this->currentRowCount = $this->vistorRepo->getListCount();
        if ($this->currentRowCount > $this->maxRowCount) {
            return ['error' => 'Maximum row size exceeded.Try with a lesser date range or filters.'];
        }

        $currentDateTime = Carbon::now()->format('Y-m-d_His'); // Get current date and time in the desired format
        $filename = "visitors_{$currentDateTime}.xlsx"; // Construct the filename

        $path = 'public/downloads/' . $filename; // Use 'public' disk

        Excel::store($this, $path, 'local'); // Store the Excel file
        // Generate a public URL for the stored file
        $fileUrl = asset('storage/downloads/' . $filename);

        return ['file_url' => $fileUrl];
    }
}

