<?php

namespace App\Http\Controllers;

use App\Models\Flats;
use App\Models\GuestUser;
use App\Models\PurposeFormFields;
use App\Models\PurposeOfVisit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Storage;

class VisitorController extends Controller
{
    public function visitor_purpose_entry(Request $request)
    {
        $data['vist_purposes'] = PurposeOfVisit::getAll();
        return view('guest.visitor_purpose_entry', $data);
    }
    public function visitor_purpose_form(Request $request, $purpose_id)
    {
        $data['vist_purpose'] = PurposeOfVisit::find($purpose_id);
        $data['purpose_form_fields'] = PurposeFormFields::where([['deleted_at', '=', null], ['p_v_id', '=', $purpose_id]])->orderBy('form_order', 'ASC')->get();
        $data['my_flats'] = Flats::select('flat_id as id', 'flat_num as text')->where([['status', '=', 1], ['building_id', '=', Session::get('building_id')]])->orderBy('flat_num', 'ASC')->get();
        return view('guest.visitor_purpose_form', $data);
    }
    public function visitor_save(Request $request)
    {
        $purpose_form_fields = PurposeFormFields::where([['deleted_at', '=', null], ['p_v_id', '=', $request->purpose_id]])->orderBy('form_order', 'ASC')->get();
        /************************************************* */// validate
        // just sample, not really added all fields for validation
        $niceNames = [
            'full_name' => 'Guest Name',
        ];
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
        ], [], $niceNames);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        /************************************************* */
        $guest_user = new GuestUser();
        /********************************************* */
        // customized data
        $guest_user->p_v_id = $request->purpose_id;
        $guest_user->purpose = $request->purpose_name;
        $guest_user->create_date = date('Y-m-d H:m:s');
        $guest_user->building_id = Session::get('building_id');
        $guest_user->accepted_status = 1;
        $guest_user->image = null;
        $guest_user->status = 1;
        $guest_user->device_id = null;
        $guest_user->device_type = null;
        $guest_user->qrcodeDetail = null;
        /********************************************* */
        if ($request->visitor_image) {
            $image = "user_photo_" . time() . ".jpg";
            Storage::disk('public')->put('uploads/images/visitors/' . $image, file_get_contents($request->visitor_image));
            $guest_user->image = $image;
        }
        /********************************************* */
        foreach ($purpose_form_fields as $key => $form_field) {
            if ($form_field->table_name == "guest_user") {
                // for guest_user table update
                $guest_user->{$form_field->table_column} = $request->{$form_field->field_name} ?: null;
                if ($form_field->field_input_type == "date") {
                    $guest_user->{$form_field->table_column} = $request->{$form_field->field_name} ? Carbon::createFromFormat('d/m/Y', $request->{$form_field->field_name})->format('Y-m-d') : null;
                }
            }
        }
        $guest_user->save();
        /************************************************* */
        $data['guest_user'] = $guest_user;
        return response()->json(['status' => true, 'data' => $data, 'message' => 'Visitor added succesfully!']);
    }
}
