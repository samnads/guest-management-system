<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;

class RedirectController extends Controller
{
    public function redirect_url(Request $request)
    {
        /************************************************** */
        switch ($request->source) {
            case ('qr_code'):
                switch ($request->action) {
                    case ('visitors_lobby'):
                        if ($request->building) {
                            Session::put('header', 1);
                            return redirect()->route('lobby', ['building_id' => $request->building]);
                        }
                        $error = 'Building id required !';
                        break;
                    default:
                        $error = 'Action required !';
                }
                break;
            default:
                $error = 'Source required !';
        }
        return $error;
    }
}
