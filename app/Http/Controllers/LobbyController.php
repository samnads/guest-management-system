<?php

namespace App\Http\Controllers;

use App\Models\Buildings;
use Illuminate\Http\Request;
use Session;

class LobbyController extends Controller
{
    public function lobby(Request $request, $building_id = null)
    {
        //$request->session()->flush();
        $data['building'] = Buildings::getOne($building_id);
        if (!$building_id) {
            return "No Building Selected !";
        }
        else if (!$data['building']) {
            return "Building Not Found !";
        }
        else if ($data['building']->status != 1) {
            return "Building Not Active !";
        }
        /******************************************* */
        // building validation done
        // perform tasks
        Session::put('building_id', $building_id);
        //Session::put('header', true);
        //dd(Session::get('header'));
        /******************************************* */
        return view('guest.lobby', $data);
    }
}
