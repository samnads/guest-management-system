<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\FlatRepository as FlatRepo;
use App\DataTables\FlatDataTable;
use Illuminate\Support\Facades\Validator;

class FlatController extends Controller
{
    protected $flatRepository;
    public function __construct()
    {
        $this->flatRepository = new FlatRepo();
    }

    //==========================================================
    public function index(Request $request)
    {
        return view('admin.flats.index');
    }

    //==========================================================
    public function flatList(Request $request)
    {
        $flatDataTable = new FlatDataTable();
        $datatable = $flatDataTable->get($request);
        return $datatable;
    }

    //==========================================================
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'flat_num' => 'required|max:50',
            'building_id' => 'required|exists:buildings,building_id',
            'username' => 'required|max:100',
            'password' => 'required|max:100',
            // Add more validation rules as needed
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $flatData = [
            'flat_num' => $request->input('flat_num'),
            'building_id' => $request->input('building_id'),
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            // Add more data fields as needed
            'status' => 1
        ];

        // Create and save the flat record using the repository
        $flat = $this->flatRepository->create($flatData);

        return response()->json(['message' => 'Flat added successfully']);
    }

    //==========================================================
    public function update(Request $request, $flatId)
    {
        $data = $request->all();
        $flat = $this->flatRepository->update($flatId, $data);
        if ($flat) {
            return response()->json($flat, 200);
        } else {
            return response()->json(['message' => 'Flat not found'], 404);
        }
    }

    //==========================================================
    public function delete($flatId)
    {
        if ($this->flatRepository->delete($flatId)) {
            return response()->json(['message' => 'Flat deleted'], 200);
        } else {
            return response()->json(['message' => 'Flat not found'], 404);
        }
    }
}
