<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\VistorRepository as VistorRepo;
use App\Services\VisitorExportExcelService as ExportExcel;
use Illuminate\Http\Request;
use Carbon\Carbon;

class VisitorController extends Controller
{
    protected $vistorRepo;
    protected $exportExcel;

    public function __construct()
    {
        $this->vistorRepo = new VistorRepo();
        $this->exportExcel = new ExportExcel();
    }

    /**====================================================
     * Function to accept Visitor
    ======VisitorController================================================= */
    public function accept(Request $request, $userId)
    {
        try {
            // Call the repository function to update the accepted status
            $this->vistorRepo->acceptVisitor($userId);

            return response()->json(['success' => true], 200);
        } catch (\Exception $ex) {
            // Handle the exception
            return response()->json(['error' => 'An error occurred.'], 500);
        }
    }

    /**====================================================
     * Function to reject Visitor
    ======================================================= */
    public function reject(Request $request, $userId)
    {
        try {
            // Call the repository function to update the accepted status
            $this->vistorRepo->rejectVisitor($userId);

            return response()->json(['success' => true], 200);
        } catch (\Exception $ex) {
            // Handle the exception
            return response()->json(['error' => 'An error occurred.'], 500);
        }
    }

    /**====================================================
     * Function to export vistor data
    ======================================================= */
    public function export(Request $request)
    {
        $exportType = $request->exportType;
        if($exportType == 'Excel'){
            return $this->exportExcel($request);
        }
    }

    /**====================================================
     * Function to export vistor data as excel
    ======================================================= */
    private function exportExcel(Request $request)
    {
        $filters = $request->filters;
        return $this->exportExcel->export($filters);
    }

    /**====================================================
     * Function to export vistor data
    ======================================================= */
    public function exportTop(Request $request)
    {
        $exportType = $request->exportType;
        if($exportType == 'Excel'){
            return $this->exportExcel($request);
        }
    }
}
