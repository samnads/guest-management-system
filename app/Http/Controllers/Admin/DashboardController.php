<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\VisitorService;

class DashboardController extends Controller
{
    protected $visitorService;

    public function __construct()
    {
        $this->visitorService = new VisitorService();
    }
    /**====================================================
     * Function to show dashboard view
    ======================================================= */
    public function index()
    {
        $vistorcounts = $this->visitorService->getVisitorsCount();
        $last24hours = $this->visitorService->getLast24HoursVisitorsCountByHour();
        $visitorByFlat = $this->visitorService->getVisitorCountByFlat();
        $visitorByPurpose = $this->visitorService->getVisitorCountByPurpose();
        $visitorByPurposeLastMonth = $this->visitorService->getVisitorCountByPurposeLastMonth();
        return view('admin.dashboard', compact(
            'vistorcounts', 'last24hours','visitorByFlat','visitorByPurpose','visitorByPurposeLastMonth'
        ));
    }
}
