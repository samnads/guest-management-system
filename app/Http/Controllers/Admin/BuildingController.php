<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\BuildingRepository as BuildingRepo;
use App\DataTables\BuildingDataTable;
use Illuminate\Support\Facades\Validator;

class BuildingController extends Controller
{
    protected $buildingRepository;

    public function __construct()
    {
        $this->buildingRepository = new BuildingRepo();
    }
    //==========================================================
    public function index(Request $request)
    {
        return view('admin.buildings.index');
    }
    //==========================================================
    public function buildingList(Request $request)
    {
        $buildingDataTable = new BuildingDataTable();
        $datatable =  $buildingDataTable->get($request);
        return $datatable;
    }
    //==========================================================
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'building_name' => 'required|max:50',
            'building_code' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $buildingData = [
            'building_name' => $request->input('building_name'),
            'code' => $request->input('building_code'),
            'status' => 1
        ];

        // Create and save the building record using the repository
        $building = $this->buildingRepository->create($buildingData);

        return response()->json(['message' => 'Building added successfully']);
    }
    //==========================================================
    public function update(Request $request, $buildingId)
    {
        $data = $request->all();
        $building = $this->buildingRepository->update($buildingId, $data);
        if ($building) {
            return response()->json($building, 200);
        } else {
            return response()->json(['message' => 'Building not found'], 404);
        }
    }
    //==========================================================
    public function delete($buildingId)
    {
        if ($this->buildingRepository->delete($buildingId)) {
            return response()->json(['message' => 'Building deleted'], 200);
        } else {
            return response()->json(['message' => 'Building not found'], 404);
        }
    }
}
