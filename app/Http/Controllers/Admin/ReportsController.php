<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataTables\VistorDataTable;
use App\DataTables\VistorDataTableTop;
use App\Repositories\VistorRepository as VistorRepo;
use App\Services\VisitorService;

class ReportsController extends Controller
{
    protected $vistorRepo;
    protected $visitorService;

    public function __construct()
    {
        $this->vistorRepo = new VistorRepo();
        $this->visitorService = new VisitorService();
    }
    /**====================================================
     * Function to return visitors data as datatable view
    ======================================================= */
    public function vistorReport()
    {
        $vistorcounts = $this->visitorService->getVisitorsCount();
        return view('admin.reports.visitors', compact('vistorcounts'));
    }
    /**====================================================
     * Function to return visitors data as datatable
    ======================================================= */
    public function vistorReportList (Request $request)
    {
        $vistorDataTable = new VistorDataTable();
        $datatable =  $vistorDataTable->get($request);
        return $datatable;
    }

    /**====================================================
     * Function to return top visitors data as datatable vie
    ======================================================= */
    public function vistorReportTop (Request $request)
    {
        $vistorcounts = $this->visitorService->getVisitorsCount();
        return view('admin.reports.visitors_top', compact('vistorcounts'));
    }
    /**====================================================
     * Function to return top visitors data as datatable
    ======================================================= */
    public function vistorReportListTop (Request $request)
    {
        $vistorDataTable = new VistorDataTableTop();
        $datatable =  $vistorDataTable->get($request);
        return $datatable;
    }


}
