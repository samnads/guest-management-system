<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function updateBuildingSession(Request $request)
    {
        $buildingId = $request->input('buildingId');
        // Update the session variable with the selected building ID
        $request->session()->put('selected_building', $buildingId);
        return response()->json(['message' => 'Session variable updated'], 200);
    }
}
