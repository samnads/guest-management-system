<?php

namespace App\Http\Controllers;

use App\Models\Buildings;
use Illuminate\Http\Request;
use Response;

class BuildingsController extends Controller
{
    public function buildings(Request $request)
    {
        $data['buildings'] = Buildings::select('building_id', 'building_name', 'code', 'status')->where([['status', '=', 1]])->get();
        $data['message'] = "Buildings fetched successfully !";
        return Response::json($data, 200, array(), JSON_PRETTY_PRINT);
    }
}
