<?php

namespace App\Repositories;

use App\Models\Buildings;
use Illuminate\Support\Facades\Log;

class BuildingRepository
{
    /**------------------------------------------------
     * PROTECTED
     --------------------------------------------------*/
     private $model;
     protected $filters = [];
     protected $orderBy;
     protected $skip;
     protected $take;
     protected $relations = [];
     protected $fields = [];
     protected $count;

    public function __construct()
    {
        $this->model = Buildings::class;
    }

    /**=====================================================
     * Function to return buildings
     * @param type $filters
     * @param type $skip
     * @param type $take
     * @param type $orderBy
     * @return type
    ===================================================== */
    public function list($filters=[],$skip=0,$take=10,$orderBy=[],$relations=[],$fields=[]){
        try{
            $this->filters = $filters;
            $this->orderBy = $orderBy;
            $this->relations = $relations;
            $this->skip = $skip;
            $this->take = $take;
            $this->fields = $fields;
            $buildings = $this->queryBuilder()->get();
            return $buildings;
        } catch (\Exception $ex) {
            Log::error($ex);
        }
    }

    /**=====================================================
     * Function to return Count
     * @return int
     =====================================================*/
    public function getListCount(){
        if(isset($this->count)){
            return $this->count;
        }
        return 0;
    }

    /**------------------------------------------------
     * PRIVATE
     --------------------------------------------------*/

    /**
     * Query builder for all basic queries
     * @return QueryBuilder
     */
    private function queryBuilder(){
        $model = $this->model;
        $queryBuilder = $model::whereNotNull('building_id');
        $this->setRelations($queryBuilder);
        $this->setFields($queryBuilder);
        $this->setFilters($queryBuilder);
        $this->count = $queryBuilder->count();
        $this->setSkip($queryBuilder);
        $this->setTake($queryBuilder);
        $this->setOrder($queryBuilder);
        return $queryBuilder;
    }

    /**
     * Set Fields to fetch with query if needed
     * @param type $query
     */
    private function setFields(&$query){
        if(!empty($this->fields)){
            $query->select($this->fields);
        }
    }

    /**
     * Set Relations to set with query
     * @param type $query
     */
    private function setRelations(&$query){
        if(!empty($this->relations)){
            foreach($this->relations as $relation){
               $query->with($relation);
            }
        }
    }

    /**
     * Set skip parameter
     * @param type $query
     */
    private function setSkip(&$query){
        $query->skip($this->skip);
    }

    /**
     * Set take parameter
     * @param type $query
     */
    private function setTake(&$query){
        if($this->take<0){
            $this->take = $this->count;
        }
        $query->take($this->take);
    }

    /**
     * Set filter parameter
     * @param type $query
     */
    private function setFilters(&$query){
        $query->where('status',1); // Not deleted buildings

        if(isset($this->filters['search']) && $this->filters['search']!==null){
            $this->search($this->filters['search'],$query);
        }
        unset($this->filters['search']);

        if(isset($this->filters)){
            foreach($this->filters as $filterKey => $filterVal){
                if(is_array($filterVal)){
                    $query->whereIn($filterKey,$filterVal);
                }else{
                    $query->where($filterKey,$filterVal);
                }
            }
        }
    }

    /**
     * Search functionality
     * @param string $searchVal
     * @param type $query
     */
    private function search(string $searchVal,&$query){
        $query->where(function ($query) use ($searchVal) {
            $query->orWhere('building_name', 'like', '%' . $searchVal . '%');
            $query->orWhere('code', 'like', '%' . $searchVal . '%');
        });
    }

    /**
     * Set filter parameter
     * @param type $query
     */
    private function setOrder(&$query){
        if (empty($this->orderBy)) {
            $query->orderBy('building_id', 'desc');
        } else {
            $orderBy = $this->orderBy;
            $query->orderBy($orderBy[0][0], $orderBy[0][1]);
        }
    }
    //====================== CRUD OPERATIONS ==================================
    public function create($data)
    {
        return Buildings::create($data);
    }
    //========================================================================
    public function update($buildingId, $data)
    {
        $building = Buildings::find($buildingId);
        if ($building) {
            $building->update($data);
            return $building;
        }
        return null;
    }
    //========================================================================
    public function delete($buildingId)
    {
        $building = Buildings::find($buildingId);
        if ($building) {
            $building->status = 0; // Set the status to 0 (inactive)
            $building->save();
            return true;
        }
        return false;
    }
}
