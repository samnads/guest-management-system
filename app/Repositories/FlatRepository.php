<?php

namespace App\Repositories;

use App\Models\Flats;
use Illuminate\Support\Facades\Log;

class FlatRepository
{
/**------------------------------------------------
 * PROTECTED
 --------------------------------------------------*/
 private $model;
 protected $filters = [];
 protected $orderBy;
 protected $skip;
 protected $take;
 protected $relations = [];
 protected $fields = [];
 protected $count;

 public function __construct()
 {
     $this->model = Flats::class; // Update with the Flat model class
 }

 /**=====================================================
  * Function to return flats
  * @param type $filters
  * @param type $skip
  * @param type $take
  * @param type $orderBy
  * @return type
 ===================================================== */
 public function list($filters = [], $skip = 0, $take = 10, $orderBy = [], $relations = [], $fields = [])
 {
     try {
         $this->filters = $filters;
         $this->orderBy = $orderBy;
         $this->relations = $relations;
         $this->skip = $skip;
         $this->take = $take;
         $this->fields = $fields;
         $flats = $this->queryBuilder()->get();
         return $flats;
     } catch (\Exception $ex) {
         Log::error($ex);
     }
 }

  /**=====================================================
     * Function to return Count
     * @return int
     =====================================================*/
     public function getListCount(){
        if(isset($this->count)){
            return $this->count;
        }
        return 0;
    }

    /**------------------------------------------------
     * PRIVATE
     --------------------------------------------------*/

    /**
     * Query builder for all basic queries
     * @return QueryBuilder
     */
    private function queryBuilder(){
        $model = $this->model;
        $queryBuilder = $model::whereNotNull('flat_id');
        $this->setRelations($queryBuilder);
        $this->setFields($queryBuilder);
        $this->setFilters($queryBuilder);
        $this->count = $queryBuilder->count();
        $this->setSkip($queryBuilder);
        $this->setTake($queryBuilder);
        $this->setOrder($queryBuilder);
        return $queryBuilder;
    }

    /**
     * Set Fields to fetch with query if needed
     * @param type $query
     */
    private function setFields(&$query){
        if(!empty($this->fields)){
            $query->select($this->fields);
        }
    }

    /**
     * Set Relations to set with query
     * @param type $query
     */
    private function setRelations(&$query){
        if(!empty($this->relations)){
            foreach($this->relations as $relation){
               $query->with($relation);
            }
        }
    }

    /**
     * Set skip parameter
     * @param type $query
     */
    private function setSkip(&$query){
        $query->skip($this->skip);
    }

    /**
     * Set take parameter
     * @param type $query
     */
    private function setTake(&$query){
        if($this->take<0){
            $this->take = $this->count;
        }
        $query->take($this->take);
    }

    /**
     * Set filter parameter
     * @param type $query
     */
    private function setFilters(&$query){
        $query->where('status',1); // Not deleted buildings

        if(isset($this->filters['search']) && $this->filters['search']!==null){
            $this->search($this->filters['search'],$query);
        }
        unset($this->filters['search']);

        if(isset($this->filters)){
            foreach($this->filters as $filterKey => $filterVal){
                if(is_array($filterVal)){
                    $query->whereIn($filterKey,$filterVal);
                }else{
                    $query->where($filterKey,$filterVal);
                }
            }
        }
    }

    /**
     * Search functionality
     * @param string $searchVal
     * @param type $query
     */
    private function search(string $searchVal,&$query){
        $query->where(function ($query) use ($searchVal) {
            $query->orWhere('flat_num', 'like', '%' . $searchVal . '%');
            $query->orWhere('username', 'like', '%' . $searchVal . '%');
            $query->orWhereHas('building', function ($query) use ($searchVal) {
                $query->where('building_name', 'like', '%' . $searchVal . '%');
            });
        });
    }

    /**
     * Set filter parameter
     * @param type $query
     */
    private function setOrder(&$query){
        if (empty($this->orderBy)) {
            $query->orderBy('flat_id', 'desc');
        } else {
            $orderBy = $this->orderBy;
            $query->orderBy($orderBy[0][0], $orderBy[0][1]);
        }
    }

 //====================== CRUD OPERATIONS ==================================
 public function create($data)
 {
     return Flats::create($data); // Use the Flat model to create
 }
 //========================================================================
 public function update($flatId, $data)
 {
     $flat = Flats::find($flatId); // Use the Flat model to find
     if ($flat) {
         $flat->update($data);
         return $flat;
     }
     return null;
 }
 //========================================================================
 public function delete($flatId)
 {
     $flat = Flats::find($flatId); // Use the Flat model to find
     if ($flat) {
         $flat->status = 0; // Set the status to 0 (inactive)
         $flat->save();
         return true;
     }
     return false;
 }
}
