<?php

namespace App\Repositories;

use App\Models\GuestUser;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class VistorRepository
{
    /**------------------------------------------------
     * PROTECTED
     --------------------------------------------------*/
    private $model;
    protected $filters = [];
    protected $orderBy;
    protected $skip;
    protected $take;
    protected $relations = [];
    protected $fields = [];
    protected $count;
    protected $isTop = false;

    public function __construct()
    {
        $this->model = GuestUser::class;
    }
    /**=====================================================
     * Function to return vistors
     * @param type $filters
     * @param type $skip
     * @param type $take
     * @param type $orderBy
     * @return type
    ===================================================== */
    public function list($filters = [], $skip = 0, $take = 10, $orderBy = [], $relations = [], $fields = [])
    {
        try {
            /**$query = "SELECT gu.user_id, gu.name, visits.Visits, gu.mobile, gu.building_id AS Building, gu.flat_id AS Flat, gu.purpose, gu.status AS Status, gu.create_date AS Entry_Date FROM guest_user gu JOIN ( SELECT mobile, purpose, COUNT(*) AS Visits, MAX(user_id) AS latest_user_id FROM guest_user GROUP BY mobile, purpose ) visits ON gu.mobile = visits.mobile AND gu.purpose = visits.purpose AND gu.user_id = visits.latest_user_id ORDER BY visits.Visits";
            //dd($query);
            $visitsData = DB::select($query);
            return $visitsData;*/


            $this->filters = $filters;
            $this->orderBy = $orderBy;
            $this->relations = $relations;
            $this->skip = $skip;
            $this->take = $take;
            $this->fields = $fields;
            if (isset($this->filters['listTop']) && $this->filters['listTop'] == true) {
                $this->isTop = true;
            }
            unset($this->filters['listTop']);
            $vistors = $this->queryBuilder()->get();
            return $vistors;
        } catch (\Exception $ex) {
            Log::error($ex);
        }
    }

    /**=====================================================
     * Function to return Count
     * @return int
     =====================================================*/
    public function getListCount()
    {
        if (isset($this->count)) {
            return $this->count;
        }

        return 0;
    }

    /**------------------------------------------------
     * PRIVATE
     --------------------------------------------------*/

    /**
     * Query builder for all basic queries
     * @return QueryBuilder
     */
    private function queryBuilder()
    {
        $model = $this->model;
        $queryBuilder = $model::whereNotNull('user_id');
        $this->setRelations($queryBuilder);
        $this->setFields($queryBuilder);
        $this->setFilters($queryBuilder);
        $this->count = $queryBuilder->count();
        $this->setSkip($queryBuilder);
        $this->setTake($queryBuilder);
        $this->setOrder($queryBuilder);
        return $queryBuilder;
    }

    /**
     * Set Fields to fetch with query if needed
     * @param type $query
     */
    private function setFields(&$query)
    {
        if($this->isTop == true){
            $query->join(DB::raw('(SELECT mobile, purpose, COUNT(*) AS Visits, MAX(user_id) AS latest_user_id FROM guest_user GROUP BY mobile, purpose) as visits'), function ($join) {
                    $join->on('guest_user.mobile', '=', 'visits.mobile')
                        ->on('guest_user.purpose', '=', 'visits.purpose')
                        ->on('guest_user.user_id', '=', 'visits.latest_user_id');
                })
                ->select('guest_user.user_id', 'guest_user.name', 'visits.Visits', 'guest_user.mobile', 'guest_user.building_id AS Building', 'guest_user.flat_id AS Flat', 'guest_user.purpose', 'guest_user.status AS Status', 'guest_user.create_date AS Entry_Date');
        }
        if (!empty($this->fields)) {
            $query->select($this->fields);
        }
    }

    /**
     * Set Relations to set with query
     * @param type $query
     */
    private function setRelations(&$query)
    {
        if (!empty($this->relations)) {
            foreach ($this->relations as $relation) {
                $query->with($relation);
            }
        }
    }

    /**
     * Set skip parameter
     * @param type $query
     */
    private function setSkip(&$query)
    {
        $query->skip($this->skip);
    }

    /**
     * Set take parameter
     * @param type $query
     */
    private function setTake(&$query)
    {
        if ($this->take <= 0) {
            $this->take = $this->count;
        }
        $query->take($this->take);
    }

    /**
     * Set filter parameter
     * @param type $query
     */
    private function setFilters(&$query)
    {
        //------------- Search box -----------------------
        if (isset($this->filters['search']) && $this->filters['search'] !== null) {
            $this->search($this->filters['search'], $query);
        }
        unset($this->filters['search']);

        // Custom filters
        if(isset($this->filters['custom']['from_date'])){
            $fromDate = $this->filters['custom']['from_date'];
            $query->whereDate('create_date', '>=', $fromDate);
        }

        if(isset($this->filters['custom']['to_date'])){
            $toDate = $this->filters['custom']['to_date'];
            $query->whereDate('create_date', '<=', $toDate);
        }

        if(isset($this->filters['custom']['purpose'])){
            $selectedPurpose = $this->filters['custom']['purpose'];
            $query->where('p_v_id', $selectedPurpose);
        }
        unset($this->filters['custom']);

        // Check if the selected_building is set in the session
        $selectedBuilding = session('selected_building');

        if ($selectedBuilding !== null && $selectedBuilding!==0) {
            $query->where('building_id', $selectedBuilding);
        }
        if (isset($this->filters)) {
            foreach ($this->filters as $filterKey => $filterVal) {
                if (is_array($filterVal)) {
                    $query->whereIn($filterKey, $filterVal);
                } else {
                    $query->where($filterKey, $filterVal);
                }
            }
        }
    }

    /**
     * Search functionality
     * @param string $searchVal
     * @param type $query
     */
    private function search(string $searchVal, &$query)
    {
        $query->where(function ($query) use ($searchVal) {
            $query->orWhere('name', 'like', '%' . $searchVal . '%');
            $query->orWhere('mobile', 'like', '%' . $searchVal . '%');
        });
    }

    /**
     * Set filter parameter
     * @param type $query
     */
    private function setOrder(&$query)
    {
        if (empty($this->orderBy)) {
            $query->orderBy('create_date', 'desc');
        } else {
            $orderBy = $this->orderBy;
            $query->orderBy($orderBy[0][0], $orderBy[0][1]);
        }
    }

    /**====================================================
     * Function to
    ======================================================= */
    public function acceptVisitor($userId)
    {
        try {
            $visitor = GuestUser::findOrFail($userId);
            $visitor->accepted_status = 1; // Assuming 1 represents 'Accepted'
            $visitor->save();
        } catch (\Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function rejectVisitor($userId)
    {
        try {
            $visitor = GuestUser::findOrFail($userId);
            $visitor->accepted_status = 2; // Assuming 1 represents 'Accepted'
            $visitor->save();
        } catch (\Exception $ex) {
            Log::error($ex);
            throw $ex;
        }
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getVisitorsCountByDate($date)
    {
        return GuestUser::whereDate('create_date', $date)->count();
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getVisitorsCountByDateRange($startDate, $endDate)
    {
        return GuestUser::whereBetween('create_date', [$startDate, $endDate])->count();
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getTotalVisitorsCount()
    {
        return GuestUser::count();
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getVisitorsCountByHourRange()
    {
        $currentDate = Carbon::now()->toDateString();
        $selectedBuilding = session('selected_building');
        $query = GuestUser::selectRaw('HOUR(create_date) AS hour')
            ->selectRaw('COUNT(*) AS count')
            ->whereDate('create_date', $currentDate);

        if ($selectedBuilding !== null) {
            $query->where('building_id', $selectedBuilding);
        }

        $result = $query->groupByRaw('HOUR(create_date)')
            ->orderByRaw('HOUR(create_date)')
            ->get();

        return $result;
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getVisitorCountByFlat()
    {
        $query = GuestUser::selectRaw('flats.flat_num AS flat_num')
            ->selectRaw('COUNT(*) AS count')
            ->leftJoin('flats', 'guest_user.flat_id', '=', 'flats.flat_id')
            ->groupBy('flats.flat_num');

        $selectedBuilding = session('selected_building');
        if ($selectedBuilding !== null) {
            $query->where('flats.building_id', $selectedBuilding);
        }

        return $query->pluck('count', 'flat_num')->toArray();
    }
    /**====================================================
     * Function to
    ======================================================= */
    public function getVisitorCountByPurpose()
    {
        $query = GuestUser::selectRaw('purpose_of_visit.purpose AS purpose')
            ->selectRaw('COUNT(*) AS count')
            ->leftJoin('purpose_of_visit', 'guest_user.p_v_id', '=', 'purpose_of_visit.p_v_id')
            ->groupBy('purpose_of_visit.purpose');

        $selectedBuilding = session('selected_building');
        if ($selectedBuilding !== null) {
            $query->where('guest_user.building_id', $selectedBuilding);
        }

        return $query->pluck('count', 'purpose')->toArray();
    }
    /**====================================================
     * Function to get visitor count by purpose for the last month
     ======================================================= */
    public function getVisitorCountByPurposeLastMonth()
    {
        $lastMonthStartDate = Carbon::today()->subMonth();

        $query = GuestUser::selectRaw('purpose_of_visit.purpose AS purpose')
            ->selectRaw('COUNT(*) AS count')
            ->leftJoin('purpose_of_visit', 'guest_user.p_v_id', '=', 'purpose_of_visit.p_v_id')
            ->whereDate('guest_user.create_date', '>=', $lastMonthStartDate)
            ->groupBy('purpose_of_visit.purpose');

        $selectedBuilding = session('selected_building');
        if ($selectedBuilding !== null) {
            $query->where('guest_user.building_id', $selectedBuilding);
        }

        return $query->pluck('count', 'purpose')->toArray();
    }
}
