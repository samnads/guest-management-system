<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuestUser extends Model
{
    use HasFactory;
    protected $table = 'guest_user';
    public $timestamps = false;
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'name', 'mobile', 'qatarid', 'location', 'description', 'purpose', 'p_v_id',
        'building_id', 'flat_id', 'image', 'create_date', 'accepted_status', 'device_id',
        'device_type', 'status', 'qrcodeDetail', 'idnumber', 'id_expiry_date', 'company_name'
    ];

    // Relationships
    public function purposeOfVisit()
    {
        return $this->belongsTo(PurposeOfVisit::class, 'p_v_id', 'p_v_id');
    }

    public function building()
    {
        return $this->belongsTo(Buildings::class, 'building_id', 'building_id');
    }

    public function flat()
    {
        return $this->belongsTo(Flats::class, 'flat_id', 'flat_id');
    }
}
