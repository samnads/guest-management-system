<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buildings extends Model
{
    use HasFactory;
    protected $table = 'buildings';
    public $timestamps = false;
    protected $primaryKey = 'building_id';
    protected $fillable = [
        'building_name',
        'code',
        'status'
    ];
    public function scopeGetOne($query, $building_id)
    {
        return $query->select('building_id', 'building_name', 'code', 'device_type', 'device_id', 'is_default', 'status')->where([['building_id', '=', $building_id]])->first();
    }
}
