<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flats extends Model
{
    use HasFactory;
    protected $table = 'flats';
    public $timestamps = false;
    protected $primaryKey = 'flat_id';
    protected $fillable = [
        'flat_num',
        'building_id',
        'username',
        'password',
    ];

    public function building()
    {
        return $this->belongsTo(Buildings::class, 'building_id');
    }
}
