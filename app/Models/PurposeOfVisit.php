<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurposeOfVisit extends Model
{
    use HasFactory;
    protected $table = 'purpose_of_visit';
    public $timestamps = false;
    protected $primaryKey = 'p_v_id';
    public function scopeGetAll($query)
    {
        return $query->select('p_v_id', 'purpose', 'status')->where([['status', '=', 1]])->orderBy('sort_order', 'ASC')->get();
    }
    public function scopeGetOne($query, $purpose_id)
    {
        return $query->select('p_v_id', 'purpose', 'status')->where([['status', '=', 1], ['p_v_id', '=', $purpose_id]])->first();
    }
}



