<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurposeFormFields extends Model
{
    use HasFactory;
    protected $table = 'purpose_form_fields';
    public $timestamps = true;
    protected $primaryKey = 'purpose_form_field_id';
}
