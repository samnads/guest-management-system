<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Include your CSS -->
    <link href="{{ asset('admin/css/app.css') }}" rel="stylesheet">
    <!-- Include your custom CSS file -->
    <link href="{{ asset('admin/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/bootstrap.css') }}" />
    <!-- DataTables CSS -->
    <link href="https://cdn.datatables.net/v/bs4/jszip-3.10.1/dt-1.13.6/af-2.6.0/b-2.4.1/b-colvis-2.4.1/b-html5-2.4.1/b-print-2.4.1/cr-1.7.0/date-1.5.1/fc-4.3.0/fh-3.4.0/kt-2.10.0/r-2.5.0/rg-1.4.0/rr-1.4.1/sc-2.2.0/sb-1.5.0/sp-2.2.0/sl-1.7.0/sr-1.3.0/datatables.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}" />
    @stack('styles')
    <!-- Include your JS -->
    <script src="{{ asset('admin/js/app.js') }}" defer></script>
</head>
<body>
    @include('admin.layouts.navigation')

    <!-- Page Heading -->
    {{ $header }}
    <!-- Page Content -->
    {{ $slot }}


    <!-- Include your custom JS file -->
    <script src="{{ asset('admin/js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/jquery-3.6.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
    <!-- DataTables JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/v/bs4/jszip-3.10.1/dt-1.13.6/af-2.6.0/b-2.4.1/b-colvis-2.4.1/b-html5-2.4.1/b-print-2.4.1/cr-1.7.0/date-1.5.1/fc-4.3.0/fh-3.4.0/kt-2.10.0/r-2.5.0/rg-1.4.0/rr-1.4.1/sc-2.2.0/sb-1.5.0/sp-2.2.0/sl-1.7.0/sr-1.3.0/datatables.min.js"></script>
    @stack('scripts')
    <script>
        var buildingSessionSetRoute = '{{ route('session.set.building') }}';
    </script>
    <script type="text/javascript" src="{{ asset('admin/js/footer.js') }}"></script>
</body>
</html>
