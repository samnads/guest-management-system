<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Include your custom CSS file -->
    <link href="{{ asset('admin/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/font-awesome.min.css') }}" />
</head>
<body>
@yield('content')
<footer>
    <div class="az-section">
        <a href="http://azinovatechnologies.com/" target="_blank">
            <div class="azinova-logo"></div>
        </a>
        <p class="no-padding">Powered by :</p>
        <div class="clear"></div>
    </div>
</footer>
    <!-- Include your custom JS file -->
    <script src="{{ asset('admin/js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/jquery-3.6.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
</body>
</html>
