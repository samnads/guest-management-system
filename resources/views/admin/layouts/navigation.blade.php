<header>
    <div class="row m-0">
        <div class="col-md-12 top-menu p-0">
            <ul>
                <li class="logo">
                    <img src="{{asset('admin/images/smartlobby.png') }}" alt="LOGO comes here" class="d-none d-lg-block">
                </li>
                <li class="menu-icon"><img src="{{asset('admin/images/menu.png') }}" alt=""></li>
                <li class="login-admin top-dropdown-set">
                    <div class="row m-0">
                        <div class="col w-auto admin-pic p-0"><img src="{{asset('admin/images/admin.jpg') }}" alt=""></div>
                        <div class="col w-auto admin-text">
                            <p>{{ Auth::user()->name }} <i class="fa fa-chevron-circle-down"></i></p>
                        </div>
                    </div>

                    <div class="top-dropdown">
                        <ul>
                            <li class="logout-btn">
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf

                                    <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                        <i class="fa fa-power-off"></i> {{ __('Log Out') }}
                                    </x-dropdown-link>
                                </form>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="top-search-main col-md-2">
                    <div class="top-search cm-field-main position-relative">
                        <x-building-select />
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-sm-12 main-nav p-0">
            <nav id="primary_nav_wrap">
                <ul>
                    <li>
                        <li><a href="{{ route('dashboard') }}">Dashboard</a>
                    </li>
                    <li><a href="javascript:void(0);"><i class="fa-file-text"></i> Reports</a>
                        <ul>
                            <li><a href="{{ route('admin.reports.vistor') }}">Vistor Report</a>
                            <li><a href="{{ route('admin.reports.vistor.top') }}">Top Report</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);"><i class="fa fa-cogs"></i> Masters <i class="fa fa-angle-down"></i></a>
                        <ul>
                            <li><a href="{{ route('admin.building.index') }}">Buildings</a></li>
                            <li><a href="{{ route('admin.flat.index') }}">Flats</a></li>
                        </ul>
                    </li>
                    <div class="clear"></div>
                </ul>
            </nav>
        </div>
    </div>
</header>
