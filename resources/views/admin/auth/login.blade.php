@extends('admin.layouts.guest')
<div class="col-lg-4 col-md-6 login-right d-flex mx-auto my-auto">
    <div class="login-main mx-auto">
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="col-sm-12 cm-field-main position-relative p-0">
                <i class="fa fa-user"></i>
                <x-input id="email" class="block mt-1 w-full input-field" placeholder="Email"
                type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="col-sm-12 cm-field-main position-relative p-0">
                <i class="fa fa-unlock-alt"></i>
                <x-input id="password" class="block mt-1 w-full input-field"
                                type="password" placeholder="Password"
                                name="password"
                                required autocomplete="current-password" />
            </div>
            <div class="col-sm-12 cm-field-btn p-0">
                <div class="flex items-center justify-end mt-4">
                    <x-button class="field-btn CM font-weight-normal">{{ __('Log in') }}</x-button>
                </div>
            </div>
        </form>
    </div>
</div>
