<x-admin-layout>
    <x-slot name="header">
    </x-slot>
    <x-slot name="content">

        @include('admin.popups.add-building-popup')
        @include('admin.popups.edit-building-popup')

        <div class="row cm-content-section m-0">
            <div class="col-12 page-title-main">
                <ul>
                    <li>
                        <h4 class="MyriadPro-Bold">Buildings</h4>
                    </li>
                </ul>
            </div><!--page-title-main end-->
            <div class="col-12 table-filter">
                <ul>
                    <li class="float-right text-right width-auto">
                        <div class="cm-field-main m-0">
                             <p class="d-none d-md-block">&nbsp;</p>
                             <span class="CM filter-btn show-add-member-btn"><i class="fa fa-user"></i> Add Building</span>
                        </div>
                    </li>

                </ul>
            </div>
            <div class="col-12 cm-content-main">
                <div class="col-12 table-main">
                    <!-- Table starts here ------------------------------->
                    <div class="Table-mobile-scroll">
                        <table class="Table table-top-style-box" id="building-table">
                            <thead>
                                <tr class="Heading table-head">
                                    <th class="Cell sl-number">Building ID</th>
                                    <th class="Cell sl-number">Building Name</th>
                                    <th class="Cell sl-number">Code</th>
                                    <th class="Cell sl-number">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="Row">
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!--cm-content-main end-->
        </div><!--cm-content-section end-->
        <!-- Modal for Delete Confirmation -->
        @include('admin.popups.confirm-box-popup')
        @push('scripts')
            <script>
                var listBuildingRoute = '{{ route('admin.building.datatable') }}';
                var addBuildingRoute = '{{ route('admin.building.create') }}';
                var editBuildingRoute = '{{ route('admin.building.update', ['buildingId' => ':buildingId']) }}';
                var deleteBuildingRoute = '{{ route('admin.building.delete', ['buildingId' => ':buildingId']) }}';
            </script>
            <script src="{{ asset('admin/js/custom/building.js') }}"></script>
        @endpush
    </x-slot>
</x-admin-layout>
