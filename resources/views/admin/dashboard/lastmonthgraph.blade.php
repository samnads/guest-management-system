<div class="col-lg-6 col-md-12 dashboard-graph-left">
    <div class="col-12 dashboard-graph-title">
        <h5>
            Last month
            <div class="clear"></div>
        </h5>
    </div>
    <div class="col-12 dashboard-graph-cont">
        <div class="col-12 dashboard-graph-left-set">
            <div id="bar-chart"><canvas id="lastMonth" width="500" height="400"></canvas></div>
        </div>
    </div>
</div>

