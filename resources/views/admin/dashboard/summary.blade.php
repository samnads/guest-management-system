<div class="row dashboard-tmb-wrapper m-0">
    <div class="col-lg-3 col-md-6 dashboard-tmb-main">
        <div class="col-12 blue-gradient dashboard-tmb">
            <div class="dashboard-tmb-big-icon"><i class="fa fa-users f95"></i></div>
            <div class="dashboard-tmb-title">Todays <i class="fa fa-users"></i></div>
            <div class="dashboard-tmb-number"><span>{{ $vistorcounts['today'] }}</span></div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 dashboard-tmb-main">
        <div class="col-12 green-gradient dashboard-tmb">
            <div class="dashboard-tmb-big-icon"><i class="fa fa-users f95"></i></div>
            <div class="dashboard-tmb-title">Last Week <i class="fa fa-users"></i></div>
            <div class="dashboard-tmb-number"><span>{{ $vistorcounts['lastWeek'] }}</span></div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 dashboard-tmb-main">
        <div class="col-12 yellow-gradient dashboard-tmb">
            <div class="dashboard-tmb-big-icon"><i class="fa fa-users f95"></i></div>
            <div class="dashboard-tmb-title">Last Month <i class="fa fa-users"></i></div>
            <div class="dashboard-tmb-number"><span>{{ $vistorcounts['lastMonth'] }}</span>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 dashboard-tmb-main">
        <div class="col-12 red-gradient dashboard-tmb">
            <div class="dashboard-tmb-big-icon"><i class="fa fa-users f95"></i></div>
            <div class="dashboard-tmb-title">Total Visitors <i class="fa fa-users"></i></div>
            <div class="dashboard-tmb-number"><span>{{ $vistorcounts['total'] }}</span></div>
        </div>
    </div>
</div>
