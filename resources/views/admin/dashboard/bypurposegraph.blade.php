<div class="col-lg-6 col-md-12 dashboard-graph-left">
    <div class="col-12 dashboard-graph-title">
        <h5>
            By Purpose
            <div class="clear"></div>
        </h5>
    </div>
    <div class="col-12 dashboard-graph-cont">
        <div class="col-12 dashboard-graph-left-set">
            <div id="bar-chart"><canvas id="byPurpose" width="500" height="400"></canvas></div>
        </div>
    </div>
</div>

