<div class="add-member-popup-wrapper" id="edit-member-popup-wrapper">
    <div class="add-member-popup-section d-flex">
        <div class="add-member-popup-main mx-auto">
            <div class="close-btn"><img src="images/white-close.png" alt=""></div>
            <h5 class="MyriadPro-Bold">Edit Building</h5>
            <div class="col-12 step-cont" id="cont1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Building Name</p>
                            <input class="input-field" id="building_name_edit" placeholder="" type="text">
                            <span class="text-danger" id="building_name_edit_error"></span>
                        </div>
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Code</p>
                            <input class="input-field" id="building_code_edit" placeholder="" type="text">
                            <span class="text-danger" id="building_code_edit_error"></span>
                        </div>
                        <div class="col-12 cm-field-btn p-0">
                            <input type="button" id="edit-building-btn" data-id ="" class="field-btn CM font-weight-normal w-auto" value="UPDATE">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
