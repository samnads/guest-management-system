<div class="add-member-popup-wrapper" id="add-member-popup-wrapper">
    <div class="add-member-popup-section d-flex">
        <div class="add-member-popup-main mx-auto">
            <div class="close-btn"><img src="images/white-close.png" alt=""></div>
            <h5 class="MyriadPro-Bold">Add Building</h5>
            <div class="col-12 step-cont" id="cont1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Building Name</p>
                            <input class="input-field" id="building_name" placeholder="" type="text">
                            <span class="text-danger" id="building_name_error"></span>
                        </div>
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Code</p>
                            <input class="input-field" id="building_code" placeholder="" type="text">
                            <span class="text-danger" id="building_code_error"></span>
                        </div>
                        <div class="col-12 cm-field-btn p-0">
                            <input type="button" id="add-building-btn" class="field-btn CM font-weight-normal w-auto" value="ADD">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
