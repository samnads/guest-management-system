<div class="add-member-popup-wrapper" id="edit-flat-popup-wrapper">
    <div class="add-member-popup-section d-flex">
        <div class="add-member-popup-main mx-auto">
            <div class="close-btn"><img src="images/white-close.png" alt=""></div>
            <h5 class="MyriadPro-Bold">Edit Flat</h5>
            <div class="col-12 step-cont" id="cont1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Flat Name</p>
                            <input class="input-field" id="flat_name_edit" placeholder="" type="text">
                            <span class="text-danger" id="flat_name_edit_error"></span>
                        </div>
                        <x-building-select-form select-id="building_select_form_edit" />
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Username</p>
                            <input class="input-field" id="flat_username_edit" placeholder="" type="text">
                            <span class="text-danger" id="flat_username_edit_error"></span>
                        </div>
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Password</p>
                            <input class="input-field" id="flat_password_edit" placeholder="" type="text">
                            <span class="text-danger" id="flat_password_edit_error"></span>
                        </div>
                        <div class="col-12 cm-field-btn p-0">
                            <input type="button" id="edit-flat-btn" class="field-btn CM font-weight-normal w-auto" value="UPDATE">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
