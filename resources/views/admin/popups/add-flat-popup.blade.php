<div class="add-member-popup-wrapper" id="add-flat-popup-wrapper">
    <div class="add-member-popup-section d-flex">
        <div class="add-member-popup-main mx-auto">
            <div class="close-btn"><img src="images/white-close.png" alt=""></div>
            <h5 class="MyriadPro-Bold">Add Flat</h5>
            <div class="col-12 step-cont" id="cont1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Flat Name</p>
                            <input class="input-field" id="flat_name" placeholder="" type="text">
                            <span class="text-danger" id="flat_name_error"></span>
                        </div>
                        <x-building-select-form select-id="building_select_form" />
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Username</p>
                            <input class="input-field" id="flat_username" placeholder="" type="text">
                            <span class="text-danger" id="flat_username_error"></span>
                        </div>
                        <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                            <p>Password</p>
                            <input class="input-field" id="flat_password" placeholder="" type="text">
                            <span class="text-danger" id="flat_password_error"></span>
                        </div>
                        <div class="col-12 cm-field-btn p-0">
                            <input type="button" id="save-flat-btn" class="field-btn CM font-weight-normal w-auto" value="ADD">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
