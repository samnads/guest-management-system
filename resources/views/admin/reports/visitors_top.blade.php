<x-admin-layout>
    <x-slot name="header">
    </x-slot>
    <x-slot name="content">

        <div class="row cm-content-section m-0">
            <div class="col-12 page-title-main">
                <ul>
                    <li>
                        <h4 class="MyriadPro-Bold">Top Vistors</h4>
                    </li>
                </ul>
            </div><!--page-title-main end-->
            <div class="col-12 cm-content-main">
                <div class="row dashboard-tmb-wrapper m-0">
                    <div class="col-lg-3 col-md-6 dashboard-tmb-main">
                        <div class="col-12 blue-gradient dashboard-tmb">
                            <div class="dashboard-tmb-big-icon"><i class="fa fa-users f95"></i></div>
                            <div class="dashboard-tmb-title">Todays <i class="fa fa-users"></i></div>
                            <div class="dashboard-tmb-number"><span>{{ $vistorcounts['today'] }}</span></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 dashboard-tmb-main">
                        <div class="col-12 green-gradient dashboard-tmb">
                            <div class="dashboard-tmb-big-icon"><i class="fa fa-users f95"></i></div>
                            <div class="dashboard-tmb-title">Last Week <i class="fa fa-users"></i></div>
                            <div class="dashboard-tmb-number"><span>{{ $vistorcounts['lastWeek'] }}</span></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 dashboard-tmb-main">
                        <div class="col-12 yellow-gradient dashboard-tmb">
                            <div class="dashboard-tmb-big-icon"><i class="fa fa-users f95"></i></div>
                            <div class="dashboard-tmb-title">Last Month <i class="fa fa-users"></i></div>
                            <div class="dashboard-tmb-number"><span>{{ $vistorcounts['lastMonth'] }}</span></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 dashboard-tmb-main">
                        <div class="col-12 red-gradient dashboard-tmb">
                            <div class="dashboard-tmb-big-icon"><i class="fa fa-users f95"></i></div>
                            <div class="dashboard-tmb-title">Total Visitors <i class="fa fa-users"></i></div>
                            <div class="dashboard-tmb-number"><span>{{ $vistorcounts['total'] }}</span></div>
                        </div>
                    </div>
                </div>
                <div class="col-12 table-main">
                    <div class="col-12 table-filter">
                        <ul>
                            <li>
                                <div id="basicExample" class="cm-field-main position-relative calendar m-0">
                                    <p>From Date</p>
                                    <input type="text" class="form-control input-field calendar" name="from-date"
                                        id="from-date" data-select="datepicker" autocomplete="off">
                                </div>
                            </li>
                            <li>
                                <div id="basicExample2" class="cm-field-main position-relative calendar m-0">
                                    <p>To Date</p>
                                    <input type="text" class="form-control input-field calendar" name="to-date"
                                        id="to-date" data-select="datepicker" autocomplete="off">
                                </div>
                            </li>
                            <li>
                                <div class="cm-field-main m-0">
                                    <p>Filter by Purpose</p>
                                    <x-purpose-list-dropdown
                                        selected-purpose="filterByPurpose"></x-purpose-list-dropdown>
                                </div>
                            </li>
                            <li>
                                <div class="cm-field-main select2-form-field m-0">
                                    <button type="submit" class="btn btn-primary" id="search_vistors">
                                        <i class="fa fa-search"></i> Search
                                    </button>
                                </div>
                            </li>
                            <li class="float-right text-right w-auto">
                                <div class="cm-field-main m-0">
                                    <p class="d-none d-md-block">&nbsp;</p>
                                    <span class="CM filter-btn" id="exportButton"><i class="fa fa-cloud-upload"></i>
                                        Export</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="Table-mobile-scroll">
                        <table class="Table table-top-style-box" id="visitorTopTable">
                            <thead>
                                <tr class="Heading table-head">
                                    <th class="Cell sl-number">Sl.No</th>
                                    <th class="Cell sl-number">Name</th>
                                    <th class="Cell sl-number">Visits</th>
                                    <th class="Cell sl-number">Mobile</th>
                                    <th class="Cell sl-number">Building</th>
                                    <th class="Cell sl-number">Flat</th>
                                    <th class="Cell sl-number">Purpose</th>
                                    <th class="Cell sl-number">Status</th>
                                    <th class="Cell sl-number">Entry Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="Row">
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!--cm-content-main end-->
        </div><!--cm-content-section end-->
        @push('styles')
            <!-- Additional CSS styles for this view -->
            <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
        @endpush
        @push('scripts')
            <script>
                var vistorRouteTop = '{{ route('admin.reports.vistor.datatable.top') }}';
                var exportRouteTop = '{{ route('admin.visitor.export.top') }}';
            </script>
            <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
            <script src="{{ asset('admin/js/custom/visitorsTop.js') }}"></script>
        @endpush
    </x-slot>
</x-admin-layout>
