$(function() {
    $('#visitorTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: vistorRoute,
            type: 'GET',
        },
        columns: [
            { data: 'user_id', name: 'user_id', className: 'Cell sl-number' },
            { data: 'name', name: 'name', className: 'Cell sl-number' },
            { data: 'mobile', name: 'mobile', className: 'Cell sl-number' },
            { data: 'idnumber', name: 'idnumber', className: 'Cell sl-number' },
            { data: 'id_expiry_date', name: 'id_expiry_date', className: 'Cell sl-number' },
            { data: 'company_name', name: 'company_name', className: 'Cell sl-number' },
            { data: 'building.building_name', name: 'building.building_name', className: 'Cell sl-number' },
            { data: 'flat.flat_num', name: 'flat.flat_num', className: 'Cell sl-number' },
            { data: 'purpose', name: 'purpose', className: 'Cell sl-number' },
            { data: 'create_date', name: 'create_date', className: 'Cell sl-number' },
            { data: 'accepted_status', name: 'accepted_status', className: 'Cell sl-number' },
            { data: 'action', name: 'action', className: 'Cell sl-number' },
        ]
    });

    $(".tooltip-ation-main").click(function(){
		$('.tooltip-ation-main').removeClass('active');
		$(this).toggleClass('active');
    });
});
