<x-admin-layout>
    <x-slot name="header">
    </x-slot>
    <x-slot name="content">

        @include('admin.popups.add-flat-popup')
        @include('admin.popups.edit-flat-popup')

        <div class="row cm-content-section m-0">
            <div class="col-12 page-title-main">
                <ul>
                    <li>
                        <h4 class="MyriadPro-Bold">Flats</h4>
                    </li>
                </ul>
            </div><!--page-title-main end-->
            <div class="col-12 table-filter">
                <ul>
                    <li class="float-right text-right width-auto">
                        <div class="cm-field-main m-0">
                             <p class="d-none d-md-block">&nbsp;</p>
                             <span class="CM filter-btn show-add-member-btn" id="add-flat-btn"><i class="fa fa-user"></i> Add Flat</span>
                        </div>
                    </li>

                </ul>
            </div>
            <div class="col-12 cm-content-main">
                <div class="col-12 table-main">
                    <!-- Table starts here ------------------------------->
                    <div class="Table-mobile-scroll">
                        <table class="Table table-top-style-box" id="flat-table">
                            <thead>
                                <tr class="Heading table-head">
                                    <th class="Cell sl-number">Flat ID</th>
                                    <th class="Cell sl-number">Flat Name</th>
                                    <th class="Cell sl-number">Building</th>
                                    <th class="Cell sl-number">Username</th>
                                    <th class="Cell sl-number">Password</th>
                                    <th class="Cell sl-number">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="Row">
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!--cm-content-main end-->
        </div><!--cm-content-section end-->
        <!-- Modal for Delete Confirmation -->
        @include('admin.popups.confirm-box-popup')
        @push('scripts')
            <script>
                var listFlatRoute = '{{ route('admin.flat.datatable') }}';
                var addFlatRoute = '{{ route('admin.flat.create') }}';
                var editFlatRoute = '{{ route('admin.flat.update', ['flatId' => ':flatId']) }}';
                var deleteFlatRoute = '{{ route('admin.flat.delete', ['flatId' => ':flatId']) }}';
            </script>
            <script src="{{ asset('admin/js/custom/flat.js') }}"></script>
        @endpush
    </x-slot>
</x-admin-layout>
