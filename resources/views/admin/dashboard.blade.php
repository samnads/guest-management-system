<x-admin-layout>
    <x-slot name="header">
    </x-slot>
    <x-slot name="content">
        <div class="row cm-content-section m-0">
            <div class="col-12 page-title-main">
                <ul>
                    <li>
                        <h4 class="MyriadPro-Bold">Dashboard</h4>
                    </li>
                </ul>
            </div><!--page-title-main end-->

            <div class="col-12 cm-content-main">
                @include('admin.dashboard.summary')
                <div class="row dashboard-graph-wrapper m-0">
                    @include('admin.dashboard.last24hoursgraph')
                    @include('admin.dashboard.flatvistorcountgraph')
                    @include('admin.dashboard.bypurposegraph')
                    @include('admin.dashboard.lastmonthgraph')
                </div>
            </div>
        </div>
        @push('scripts')
            <script>
                var timerange = <?php echo json_encode(array_keys($last24hours)); ?>;
                var vistors = <?php echo json_encode(array_values($last24hours)); ?>;
                var flats  = <?php echo json_encode(array_keys($visitorByFlat)); ?>;
                var visitorByFlat  = <?php echo json_encode(array_values($visitorByFlat)); ?>;
                var purposes  = <?php echo json_encode(array_keys($visitorByPurpose)); ?>;
                var visitorByPurpose  = <?php echo json_encode(array_values($visitorByPurpose)); ?>;
                var purposesLastMonth  = <?php echo json_encode(array_keys($visitorByPurposeLastMonth)); ?>;
                var visitorByPurposeLastMonth  = <?php echo json_encode(array_values($visitorByPurposeLastMonth)); ?>;
            </script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
            <script src="{{ asset('admin/js/custom/dashboard.js') }}"></script>
        @endpush
    </x-slot>
</x-admin-layout>
