<div class="form-group">
    <label for="{{ $selectId }}">Select Building:</label>
    <select name="building" id="{{ $selectId }}" class="form-control">
        <option value="">Select a building</option>
        @foreach ($buildings as $building)
            <option value="{{ $building->building_id }}">{{ $building->building_name }}</option>
        @endforeach
    </select>
    <span class="text-danger" id="{{ $selectId }}_error"></span>
</div>
