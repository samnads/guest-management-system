<div>
    <nav>
        <!-- Your navigation menu or header goes here -->
    </nav>

    <main>
        {{ $slot }}
    </main>

    <footer>
        <!-- Your footer content goes here -->
    </footer>
</div>
