<div>
    <select name="purpose" id="purpose" class="form-control">
        <option value="">Select Purpose</option>
        @foreach ($purposes as $purpose)
            <option value="{{ $purpose->p_v_id }}" @if ($selectedPurpose == $purpose->p_v_id) selected @endif>
                {{ $purpose->purpose }}
            </option>
        @endforeach
    </select>
</div>
