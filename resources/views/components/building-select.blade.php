<div class="form-group">
    <label for="buildingSelect">Building</label>
    <select id="buildingSelect" class="form-control">
        <option value="">Select from list</option>
        @foreach($buildings as $building)
            <option value="{{ $building->building_id }}"
                {{ session('selected_building') == $building->building_id ? 'selected' : '' }}>
                {{ $building->building_name }}
            </option>
        @endforeach
    </select>
</div>

