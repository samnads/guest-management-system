@extends('guest.layouts.main')
@section('title', 'Lobby')
@section('content')
<section class="v-center home-page text-center">
    <div class="row main-content m-0">
	    <div class="col-sm-12 welcome-image"><img src="{{ asset('images/lobby.png') }}" alt="" /></div>
    	<div class="home-title Metropolis-Bold"><span class="Metropolis-Regular">Welcome to</span><br />{{$building->building_name}}</div>
		<div class="n-btn-main"><a href="{{url('guest/visitor/purpose')}}" class="n-btn">Next&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
	</div><!--main-content end-->
	@include('guest.includes.footer')
</section><!--HOME Section End-->
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
