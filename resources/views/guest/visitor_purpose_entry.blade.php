@extends('guest.layouts.main')
@section('title', 'Purpose')
@section('content')
<section class="v-center purpose-visit-page text-center">
	@include('guest.includes.header', ['back_url' => url('guest/lobby/'.Session::get('building_id'))])


	<div class="row main-content m-0">

		<div class="home-title Metropolis-Bold"><span class="Metropolis-Regular">Purpose of</span> VISIT</div>
		<div class="n-btn-main">
			@foreach($vist_purposes as $key => $purpose)
			<a href="{{url('guest/visitor/purpose/'.$purpose->p_v_id.'/form?purpose='.$purpose->purpose)}}" class="n-btn">{{$purpose->purpose}}</a>
			@endforeach;
		</div>
	</div><!--main-content end-->

	<div class="col-12 qrcode-main">
	    <div class="qrcode"><img src="{{ asset('images/qrcode.png') }}" alt="" /></div>
	</div>

	@include('guest.includes.footer')
</section><!--Purpose of Visit Section End-->
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
