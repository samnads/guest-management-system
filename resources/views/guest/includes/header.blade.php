@if(Session::get('header') == 1)
<header>
    <div class="row m-0">
        <div class="col-6 back-btn-main p-0">
            <a href="{{ @$back_url ?: '#' }}">
                <div class="back-btn pull-left goto-purpose-visit-btn">Back</div>
            </a>
        </div>
        <div class="col-6 p-0 text-white text-right">Time</div>
    </div>
</header>
@endif
