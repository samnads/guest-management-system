<link rel="icon" type="image/png" href="images/logo-icon.png"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css?v=1.0') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/animation.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css?v='.time()) }}"/>