@extends('guest.layouts.main')
@section('title', 'Purpose Details')
@section('content')
    <section class="v-center guest-page">
        @include('guest.includes.header', ['back_url' => url('guest/visitor/purpose')])
        <form id="guest_add_form" method="post" enctype="multipart/form-data">
            <input type="hidden" type="number" name="purpose_id" value="{{ $vist_purpose->p_v_id }}">
            <input type="hidden" type="text" name="purpose_name" value="{{ $vist_purpose->purpose }}">
            <div class="row main-content m-0">
                <div class="col-lg-8 col-md-12 m-cont-left p-0">
                    <div class="row m-0">
                        @foreach ($purpose_form_fields as $key => $form_field)
                            <div class="col-md-6 col-sm-6 cm-field-main">
                                <div class="field-icon v-center"><img
                                        src="{{ asset('images/input-field-icons/' . ($form_field->field_icon_file ?: 'default.png')) }}" />
                                </div>
                                <p>{{ $form_field->field_label }}{!! $form_field->field_is_required == 1 ? '<rf>' : '' !!}</p>
                                @if ($form_field->field_type == 'input')
                                    <input class="input-field w90 {{ $form_field->field_css_classes }}"
                                        placeholder="{{ $form_field->field_placeholder }}"
                                        name="{{ $form_field->field_name }}"
                                        type="{{ $form_field->field_input_type != 'date' ? $form_field->field_input_type : ($form_field->custom_date_picker == 1 ? 'text' : 'date') }}"
                                        autocomplete="off" {{ $form_field->field_input_type == 'date' ? 'readonly' : '' }}
                                        style="{{ $form_field->field_inline_css }}" id="{{ $form_field->field_id }}">
                                @elseif($form_field->field_type == 'textarea')
                                    <textarea class="input-field w90 {{ $form_field->field_css_classes }}"
                                        placeholder="{{ $form_field->field_placeholder }}" name="{{ $form_field->field_name }}"
                                        type="{{ $form_field->field_input_type }}" style="{{ $form_field->field_inline_css }}"
                                        id="{{ $form_field->field_id }}"></textarea>
                                @elseif($form_field->field_type == 'select')
                                    <select class="w90" name="{{ $form_field->field_name }}"
                                        style="{{ $form_field->field_inline_css }}" id="{{ $form_field->field_id }}">
                                        <option value="">{{ $form_field->select_options_default_label }}</option>
                                        @foreach (${$form_field->select_options_variable} as $key => $option)
                                            <option value="{{ $option->id }}">{{ $option->text }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 m-cont-right p-0">
                    <div class="col-sm-12 user-photo">
                        <label for="visitor_image_upload" style="display: block;cursor:pointer;">
                            <img id="visitor_image_preview" src="{{ asset('images/user-photo.png') }}" alt="" />
                            <input type="file" name="visitor_image_upload" id="visitor_image_upload" accept="image/*"
                                style="display:none;">
                        </label>
                    </div>
                    <div class="col-sm-12 user-photo-btn">
                        <label class="cameraButton" for="visitor_image"><i class="fa fa-camera"
                                aria-hidden="true"></i>&nbsp;&nbsp;Take a
                            picture
                            <input type="file" name="visitor_image" id="visitor_image" accept="image/*" capture="user">
                        </label>
                    </div>
                </div>
                <div class="col-md-12 m-cont-bottom cm-field-main text-center p-0">
                    @if (sizeof($purpose_form_fields))
                        <input type="submit" class="n-btn" value="Submit Details">
                    @else
                        <input type="button" class="btn btn-danger" value="No data found for this purpose !">
                    @endif
                </div>
            </div><!--main-content end-->
        </form>
        <div class="col-12 qrcode-main">
            <div class="qrcode"><img src="{{ asset('images/qrcode.png') }}" alt="" /></div>
        </div>
        @include('guest.includes.footer')
    </section><!--Guest Section End-->
    <div class="common-popup-wrapper" style="display:none;">
        <div class="common-popup-section d-flex">
            <div class="common-popup-main p-0 col-md-6 col-sm-6">
                <div class="close-btn"><img src="https://booking.emaid.info:3443/elite-demo/images/close-big-w.webp"
                        alt=""></div>
                <div class="common-popup-content-main text-center m-0">
                    <div class="success-icon"><img src="{{ asset('images/az.gif') }}" alt="" /></div>
                    <h4>Your requested submitted <span class="Metropolis-Bold text-capital">successfully!</span></h4>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}" />
    <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datepicker.js') }}"></script>
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('js/main.js?v=' . jsVersion()) }}"></script>
    <!------------------------------------------------------------------------------------------------>
    <script type="text/javascript">
        var rules = {
            visitor_image: {
                required: true
            },
            @foreach ($purpose_form_fields as $key => $form_field)
                {{ $form_field->field_name }}: {
                    required: {{ $form_field->field_is_required == 1 ? 'true' : 'false' }}
                },
            @endforeach
        };
        var rules_messages = {
            visitor_image: "<br><br><br>fg",
            @foreach ($purpose_form_fields as $key => $form_field)
                {{ $form_field->field_name }}: "{{ $form_field->field_is_required_error_message }}",
            @endforeach
        };
        var building_id = {{ Session::get('building_id') }};
        @foreach ($purpose_form_fields as $key => $form_field)
            @if ($form_field->field_input_type == 'date' && $form_field->custom_date_picker == 1)
                $('#guest_add_form input[name="{{ $form_field->field_name }}"]').datepicker({
                    autoclose: true
                });
            @endif
        @endforeach
    </script>
    <!------------------------------------------------------------------------------------------------>
    <script src="{{ asset('js/visitor-purpose-form.js?v=' . jsVersion()) }}"></script>
@endpush
