<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Guest Management - @yield('title', 'Default')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/x-icon" href="{{ url('assets/images/favicon.ico') }}">
    @include('guest.includes.header_assets')
    @stack('styles')
</head>

<body>
<div class="wrapper-main">
    @yield('content')
    @include('guest.includes.footer_assets')
    @stack('scripts')
</div>
</body>
</html>
