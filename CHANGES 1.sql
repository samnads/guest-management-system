-------------------------------------------------------------------------------------
ALTER TABLE `admin_user`
ENGINE='InnoDB';
ALTER TABLE `buildings`
ENGINE='InnoDB';
ALTER TABLE `flats`
ENGINE='InnoDB';
ALTER TABLE `guest_user`
ENGINE='InnoDB';
ALTER TABLE `purpose_of_visit`
ENGINE='InnoDB';
ALTER TABLE `version_update`
ENGINE='InnoDB';
-------------------------------------------------------------------------------------
ALTER TABLE `purpose_of_visit`
ADD `sort_order` varchar(55) COLLATE 'utf8_unicode_ci' NULL AFTER `purpose`;
-------------------------------------------------------------------------------------
ALTER TABLE `guest_user`
ADD `p_v_id` int(11) NULL AFTER `purpose`,
ADD FOREIGN KEY (`p_v_id`) REFERENCES `purpose_of_visit` (`p_v_id`);
-------------------------------------------------------------------------------------
ALTER TABLE `flats`
ADD FOREIGN KEY (`building_id`) REFERENCES `buildings` (`building_id`);
-------------------------------------------------------------------------------------
ALTER TABLE `admin_user`
ADD UNIQUE `username` (`username`);
-------------------------------------------------------------------------------------
ALTER TABLE `guest_user`
CHANGE `building_id` `building_id` bigint(20) unsigned NULL AFTER `p_v_id`;
-------------------------------------------------------------------------------------
UPDATE `guest_user` SET
`building_id` = NULL
WHERE `building_id` = '0';
-------------------------------------------------------------------------------------
ALTER TABLE `guest_user`
ADD FOREIGN KEY (`building_id`) REFERENCES `buildings` (`building_id`);
-------------------------------------------------------------------------------------
ALTER TABLE `guest_user`
CHANGE `flat_id` `flat_id` bigint(20) unsigned NULL AFTER `building_id`;
-------------------------------------------------------------------------------------
UPDATE `guest_user` SET
`flat_id` = NULL
WHERE `flat_id` = '0';
-------------------------------------------------------------------------------------
ALTER TABLE `guest_user`
ADD FOREIGN KEY (`flat_id`) REFERENCES `flats` (`flat_id`);
-------------------------------------------------------------------------------------
ALTER TABLE `version_update`
ADD UNIQUE `device_type` (`device_type`),
DROP INDEX `device_type`;
-------------------------------------------------------------------------------------
ALTER TABLE `purpose_of_visit`
ADD UNIQUE `purpose` (`purpose`);
-------------------------------------------------------------------------------------
CREATE TABLE `purpose_form_fields` (
  `purpose_form_field_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `p_v_id` int(11) NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `table_column` varchar(70) NOT NULL,
  `field_type` enum('input','textarea','select') NOT NULL,
  `select_options_variable` enum('my_flats') DEFAULT NULL COMMENT 'php variable with select data',
  `select_options_default_label` varchar(155) DEFAULT NULL COMMENT 'default selected option text with null value',
  `field_input_type` enum('text','number','password','checkbox','radio','date') DEFAULT NULL,
  `custom_date_picker` tinyint(4) DEFAULT NULL,
  `field_name` varchar(70) NOT NULL,
  `field_placeholder` varchar(70) DEFAULT NULL,
  `field_id` varchar(70) DEFAULT NULL,
  `field_label` varchar(100) NOT NULL,
  `field_is_required` enum('1') DEFAULT NULL,
  `field_is_required_error_message` varchar(255) DEFAULT NULL,
  `field_icon_file` varchar(255) DEFAULT NULL,
  `field_icon_html` varchar(255) DEFAULT NULL,
  `field_css_classes` varchar(255) DEFAULT NULL COMMENT 'separate each class with space',
  `field_inline_css` varchar(255) DEFAULT NULL,
  `form_order` int(5) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`purpose_form_field_id`),
  KEY `field_type_id` (`field_type`),
  KEY `p_v_id` (`p_v_id`),
  CONSTRAINT `purpose_form_fields_ibfk_1` FOREIGN KEY (`p_v_id`) REFERENCES `purpose_of_visit` (`p_v_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `purpose_form_fields` (`purpose_form_field_id`, `p_v_id`, `table_name`, `table_column`, `field_type`, `select_options_variable`, `select_options_default_label`, `field_input_type`, `custom_date_picker`, `field_name`, `field_placeholder`, `field_id`, `field_label`, `field_is_required`, `field_is_required_error_message`, `field_icon_file`, `field_icon_html`, `field_css_classes`, `field_inline_css`, `form_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	5,	'guest_user',	'flat_id',	'select',	'my_flats',	'-- Select Flat --',	NULL,	NULL,	'flat_id',	NULL,	NULL,	'Apartment Number',	'1',	'Select apartment from list.',	'apartment.png',	NULL,	NULL,	NULL,	2,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(2,	5,	'guest_user',	'mobile',	'input',	NULL,	NULL,	'number',	NULL,	'mobile',	NULL,	NULL,	'Mobile Number',	'1',	'Enter mobile number.',	'smartphone.png',	NULL,	NULL,	NULL,	3,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(3,	5,	'guest_user',	'name',	'input',	NULL,	NULL,	'text',	NULL,	'full_name',	NULL,	NULL,	'Visitor Name',	'1',	'Enter guest name.',	'user.png',	NULL,	NULL,	NULL,	1,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(4,	5,	'guest_user',	'company_name',	'input',	NULL,	NULL,	'text',	NULL,	'company_name',	NULL,	NULL,	'Company Name',	'1',	'Enter company name.',	'office-building.png',	NULL,	NULL,	NULL,	4,	'2023-08-11 11:47:23',	'2023-08-11 11:47:23',	NULL),
(5,	5,	'guest_user',	'idnumber',	'input',	NULL,	NULL,	'text',	NULL,	'id_number',	NULL,	NULL,	'ID Number',	'1',	'Enter id number.',	'license.png',	NULL,	NULL,	NULL,	5,	'2023-08-11 11:49:03',	'2023-08-11 11:49:03',	NULL),
(6,	5,	'guest_user',	'id_expiry_date',	'input',	NULL,	NULL,	'date',	1,	'id_expiry_date',	NULL,	NULL,	'ID Expiry Date ',	'1',	'Enter id expiry date.',	'calendar.png',	NULL,	NULL,	NULL,	6,	'2023-08-11 11:52:20',	'2023-08-11 11:52:20',	NULL),
(7,	12,	'guest_user',	'flat_id',	'select',	'my_flats',	'-- Select Flat --',	NULL,	NULL,	'flat_id',	NULL,	NULL,	'Apartment Number',	'1',	'Select apartment from list.',	'apartment.png',	NULL,	NULL,	NULL,	2,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(8,	12,	'guest_user',	'mobile',	'input',	NULL,	NULL,	'number',	NULL,	'mobile',	NULL,	NULL,	'Mobile Number',	'1',	'Enter mobile number.',	'smartphone.png',	NULL,	NULL,	NULL,	3,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(9,	12,	'guest_user',	'name',	'input',	NULL,	NULL,	'text',	NULL,	'full_name',	NULL,	NULL,	'Visitor Name',	'1',	'Enter guest name.',	'user.png',	NULL,	NULL,	NULL,	1,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(10,	12,	'guest_user',	'company_name',	'input',	NULL,	NULL,	'text',	NULL,	'company_name',	NULL,	NULL,	'Company Name',	'1',	'Enter company name.',	'office-building.png',	NULL,	NULL,	NULL,	4,	'2023-08-11 11:47:23',	'2023-08-11 11:47:23',	NULL),
(11,	12,	'guest_user',	'idnumber',	'input',	NULL,	NULL,	'text',	NULL,	'id_number',	NULL,	NULL,	'ID Number',	'1',	'Enter id number.',	'license.png',	NULL,	NULL,	NULL,	5,	'2023-08-11 11:49:03',	'2023-08-11 11:49:03',	NULL),
(12,	12,	'guest_user',	'id_expiry_date',	'input',	NULL,	NULL,	'date',	1,	'id_expiry_date',	NULL,	NULL,	'ID Expiry Date ',	'1',	'Enter id expiry date.',	'calendar.png',	NULL,	NULL,	NULL,	6,	'2023-08-11 11:52:20',	'2023-08-11 11:52:20',	NULL),
(14,	6,	'guest_user',	'flat_id',	'select',	'my_flats',	'-- Select Flat --',	NULL,	NULL,	'flat_id',	NULL,	NULL,	'Apartment Number',	'1',	'Select apartment from list.',	'apartment.png',	NULL,	NULL,	NULL,	2,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(15,	6,	'guest_user',	'mobile',	'input',	NULL,	NULL,	'number',	NULL,	'mobile',	NULL,	NULL,	'Mobile Number',	'1',	'Enter mobile number.',	'smartphone.png',	NULL,	NULL,	NULL,	3,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(16,	6,	'guest_user',	'name',	'input',	NULL,	NULL,	'text',	NULL,	'full_name',	NULL,	NULL,	'Visitor Name',	'1',	'Enter guest name.',	'user.png',	NULL,	NULL,	NULL,	1,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(17,	6,	'guest_user',	'company_name',	'input',	NULL,	NULL,	'text',	NULL,	'company_name',	NULL,	NULL,	'Company Name',	'1',	'Enter company name.',	'office-building.png',	NULL,	NULL,	NULL,	4,	'2023-08-11 11:47:23',	'2023-08-11 11:47:23',	NULL),
(18,	6,	'guest_user',	'idnumber',	'input',	NULL,	NULL,	'text',	NULL,	'id_number',	NULL,	NULL,	'ID Number',	'1',	'Enter id number.',	'license.png',	NULL,	NULL,	NULL,	5,	'2023-08-11 11:49:03',	'2023-08-11 11:49:03',	NULL),
(19,	6,	'guest_user',	'id_expiry_date',	'input',	NULL,	NULL,	'date',	1,	'id_expiry_date',	NULL,	NULL,	'ID Expiry Date ',	'1',	'Enter id expiry date.',	'calendar.png',	NULL,	NULL,	NULL,	6,	'2023-08-11 11:52:20',	'2023-08-11 11:52:20',	NULL),
(21,	7,	'guest_user',	'flat_id',	'select',	'my_flats',	'-- Select Flat --',	NULL,	NULL,	'flat_id',	NULL,	NULL,	'Apartment Number',	'1',	'Select apartment from list.',	'apartment.png',	NULL,	NULL,	NULL,	2,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(22,	7,	'guest_user',	'mobile',	'input',	NULL,	NULL,	'number',	NULL,	'mobile',	NULL,	NULL,	'Mobile Number',	'1',	'Enter mobile number.',	'smartphone.png',	NULL,	NULL,	NULL,	3,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(23,	7,	'guest_user',	'name',	'input',	NULL,	NULL,	'text',	NULL,	'full_name',	NULL,	NULL,	'Visitor Name',	'1',	'Enter guest name.',	'user.png',	NULL,	NULL,	NULL,	1,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(24,	7,	'guest_user',	'company_name',	'input',	NULL,	NULL,	'text',	NULL,	'company_name',	NULL,	NULL,	'Company Name',	'1',	'Enter company name.',	'office-building.png',	NULL,	NULL,	NULL,	4,	'2023-08-11 11:47:23',	'2023-08-11 11:47:23',	NULL),
(25,	7,	'guest_user',	'idnumber',	'input',	NULL,	NULL,	'text',	NULL,	'id_number',	NULL,	NULL,	'ID Number',	'1',	'Enter id number.',	'license.png',	NULL,	NULL,	NULL,	5,	'2023-08-11 11:49:03',	'2023-08-11 11:49:03',	NULL),
(26,	7,	'guest_user',	'id_expiry_date',	'input',	NULL,	NULL,	'date',	1,	'id_expiry_date',	NULL,	NULL,	'ID Expiry Date ',	'1',	'Enter id expiry date.',	'calendar.png',	NULL,	NULL,	NULL,	6,	'2023-08-11 11:52:20',	'2023-08-11 11:52:20',	NULL),
(28,	8,	'guest_user',	'flat_id',	'select',	'my_flats',	'-- Select Flat --',	NULL,	NULL,	'flat_id',	NULL,	NULL,	'Apartment Number',	'1',	'Select apartment from list.',	'apartment.png',	NULL,	NULL,	NULL,	2,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(29,	8,	'guest_user',	'mobile',	'input',	NULL,	NULL,	'number',	NULL,	'mobile',	NULL,	NULL,	'Mobile Number',	'1',	'Enter mobile number.',	'smartphone.png',	NULL,	NULL,	NULL,	3,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(30,	8,	'guest_user',	'name',	'input',	NULL,	NULL,	'text',	NULL,	'full_name',	NULL,	NULL,	'Visitor Name',	'1',	'Enter guest name.',	'user.png',	NULL,	NULL,	NULL,	1,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(31,	8,	'guest_user',	'company_name',	'input',	NULL,	NULL,	'text',	NULL,	'company_name',	NULL,	NULL,	'Company Name',	'1',	'Enter company name.',	'office-building.png',	NULL,	NULL,	NULL,	4,	'2023-08-11 11:47:23',	'2023-08-11 11:47:23',	NULL),
(32,	8,	'guest_user',	'idnumber',	'input',	NULL,	NULL,	'text',	NULL,	'id_number',	NULL,	NULL,	'ID Number',	'1',	'Enter id number.',	'license.png',	NULL,	NULL,	NULL,	5,	'2023-08-11 11:49:03',	'2023-08-11 11:49:03',	NULL),
(33,	8,	'guest_user',	'id_expiry_date',	'input',	NULL,	NULL,	'date',	1,	'id_expiry_date',	NULL,	NULL,	'ID Expiry Date ',	'1',	'Enter id expiry date.',	'calendar.png',	NULL,	NULL,	NULL,	6,	'2023-08-11 11:52:20',	'2023-08-11 11:52:20',	NULL),
(35,	9,	'guest_user',	'flat_id',	'select',	'my_flats',	'-- Select Flat --',	NULL,	NULL,	'flat_id',	NULL,	NULL,	'Apartment Number',	'1',	'Select apartment from list.',	'apartment.png',	NULL,	NULL,	NULL,	2,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(36,	9,	'guest_user',	'mobile',	'input',	NULL,	NULL,	'number',	NULL,	'mobile',	NULL,	NULL,	'Mobile Number',	'1',	'Enter mobile number.',	'smartphone.png',	NULL,	NULL,	NULL,	3,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(37,	9,	'guest_user',	'name',	'input',	NULL,	NULL,	'text',	NULL,	'full_name',	NULL,	NULL,	'Visitor Name',	'1',	'Enter guest name.',	'user.png',	NULL,	NULL,	NULL,	1,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(38,	9,	'guest_user',	'company_name',	'input',	NULL,	NULL,	'text',	NULL,	'company_name',	NULL,	NULL,	'Company Name',	'1',	'Enter company name.',	'office-building.png',	NULL,	NULL,	NULL,	4,	'2023-08-11 11:47:23',	'2023-08-11 11:47:23',	NULL),
(39,	9,	'guest_user',	'idnumber',	'input',	NULL,	NULL,	'text',	NULL,	'id_number',	NULL,	NULL,	'ID Number',	'1',	'Enter id number.',	'license.png',	NULL,	NULL,	NULL,	5,	'2023-08-11 11:49:03',	'2023-08-11 11:49:03',	NULL),
(40,	9,	'guest_user',	'id_expiry_date',	'input',	NULL,	NULL,	'date',	1,	'id_expiry_date',	NULL,	NULL,	'ID Expiry Date ',	'1',	'Enter id expiry date.',	'calendar.png',	NULL,	NULL,	NULL,	6,	'2023-08-11 11:52:20',	'2023-08-11 11:52:20',	NULL),
(42,	10,	'guest_user',	'flat_id',	'select',	'my_flats',	'-- Select Flat --',	NULL,	NULL,	'flat_id',	NULL,	NULL,	'Apartment Number',	'1',	'Select apartment from list.',	'apartment.png',	NULL,	NULL,	NULL,	2,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(43,	10,	'guest_user',	'mobile',	'input',	NULL,	NULL,	'number',	NULL,	'mobile',	NULL,	NULL,	'Mobile Number',	'1',	'Enter mobile number.',	'smartphone.png',	NULL,	NULL,	NULL,	3,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(44,	10,	'guest_user',	'name',	'input',	NULL,	NULL,	'text',	NULL,	'full_name',	NULL,	NULL,	'Visitor Name',	'1',	'Enter guest name.',	'user.png',	NULL,	NULL,	NULL,	1,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(45,	10,	'guest_user',	'company_name',	'input',	NULL,	NULL,	'text',	NULL,	'company_name',	NULL,	NULL,	'Company Name',	'1',	'Enter company name.',	'office-building.png',	NULL,	NULL,	NULL,	4,	'2023-08-11 11:47:23',	'2023-08-11 11:47:23',	NULL),
(46,	10,	'guest_user',	'idnumber',	'input',	NULL,	NULL,	'text',	NULL,	'id_number',	NULL,	NULL,	'ID Number',	'1',	'Enter id number.',	'license.png',	NULL,	NULL,	NULL,	5,	'2023-08-11 11:49:03',	'2023-08-11 11:49:03',	NULL),
(47,	10,	'guest_user',	'id_expiry_date',	'input',	NULL,	NULL,	'date',	1,	'id_expiry_date',	NULL,	NULL,	'ID Expiry Date ',	'1',	'Enter id expiry date.',	'calendar.png',	NULL,	NULL,	NULL,	6,	'2023-08-11 11:52:20',	'2023-08-11 11:52:20',	NULL),
(49,	11,	'guest_user',	'mobile',	'input',	NULL,	NULL,	'number',	NULL,	'mobile',	NULL,	NULL,	'Mobile Number',	'1',	'Enter mobile number.',	'smartphone.png',	NULL,	NULL,	NULL,	3,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(50,	11,	'guest_user',	'name',	'input',	NULL,	NULL,	'text',	NULL,	'full_name',	NULL,	NULL,	'Visitor Name',	'1',	'Enter guest name.',	'user.png',	NULL,	NULL,	NULL,	1,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(51,	11,	'guest_user',	'company_name',	'input',	NULL,	NULL,	'text',	NULL,	'company_name',	NULL,	NULL,	'Company Name',	'1',	'Enter company name.',	'office-building.png',	NULL,	NULL,	NULL,	4,	'2023-08-11 11:47:23',	'2023-08-11 11:47:23',	NULL),
(52,	11,	'guest_user',	'idnumber',	'input',	NULL,	NULL,	'text',	NULL,	'id_number',	NULL,	NULL,	'ID Number',	'1',	'Enter id number.',	'license.png',	NULL,	NULL,	NULL,	5,	'2023-08-11 11:49:03',	'2023-08-11 11:49:03',	NULL),
(53,	11,	'guest_user',	'id_expiry_date',	'input',	NULL,	NULL,	'date',	1,	'id_expiry_date',	NULL,	NULL,	'ID Expiry Date ',	'1',	'Enter id expiry date.',	'calendar.png',	NULL,	NULL,	NULL,	6,	'2023-08-11 11:52:20',	'2023-08-11 11:52:20',	NULL),
(56,	11,	'guest_user',	'flat_id',	'select',	'my_flats',	'-- Select Flat --',	NULL,	NULL,	'flat_id',	NULL,	NULL,	'Apartment Number',	'1',	'Select apartment from list.',	'apartment.png',	NULL,	NULL,	NULL,	2,	'2023-08-11 11:39:34',	'2023-08-11 11:39:34',	NULL),
(58,	12,	'guest_user',	'purpose',	'textarea',	NULL,	NULL,	NULL,	NULL,	'other_purpose',	'Type purpose details...',	NULL,	'Purpose Details',	NULL,	'Enter purpose details.',	NULL,	NULL,	NULL,	'min-height:80px;',	80,	'2023-08-11 15:45:08',	'2023-08-11 15:45:08',	NULL);
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
