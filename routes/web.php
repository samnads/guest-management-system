<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\LobbyController;
use App\Http\Controllers\VisitorController;
use App\Http\Controllers\RedirectController;

use App\Http\Controllers\Admin\SessionController;
use App\Http\Controllers\Admin\ReportsController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\VisitorController as AdminVisitorController;
use App\Http\Controllers\Admin\BuildingController as AdminBuildingController;
use App\Http\Controllers\Admin\FlatController as AdminFlatController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [AuthenticatedSessionController::class, 'create']);
/**===============================================================
 * Routes defined for Guest related routes
================================================================== */
Route::prefix('guest')->group(function () {
    Route::get('lobby/{building_id?}', [LobbyController::class, 'lobby'])->name('lobby');
    Route::get('visitor/purpose', [VisitorController::class, 'visitor_purpose_entry']);
    Route::get('visitor/purpose/{purpose_id}/form', [VisitorController::class, 'visitor_purpose_form']);
    Route::post('visitor/save', [VisitorController::class, 'visitor_save']);
});
Route::get('redirect', [RedirectController::class, 'redirect_url']);
/**=================================================================
 * Routes defined for Admin Portal related routes
==================================================================== */
Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/reports/vistor', [ReportsController::class, 'vistorReport'])->name('admin.reports.vistor');
    Route::get('/reports/vistor/datatable', [ReportsController::class, 'vistorReportList'])->name('admin.reports.vistor.datatable');
    Route::get('/reports/vistor/top', [ReportsController::class, 'vistorReportTop'])->name('admin.reports.vistor.top');
    Route::get('/reports/vistor/datatable/top', [ReportsController::class, 'vistorReportListTop'])->name('admin.reports.vistor.datatable.top');

    Route::patch('visitor/accept/{id}', [AdminVisitorController::class, 'accept'])->name('admin.visitor.accept');
    Route::patch('visitor/reject/{id}', [AdminVisitorController::class, 'reject'])->name('admin.visitor.reject');
    Route::post('visitor/export', [AdminVisitorController::class, 'export'])->name('admin.visitor.export');
    Route::post('visitor/export/top', [AdminVisitorController::class, 'exportTop'])->name('admin.visitor.export.top');

    Route::post('session/set/building', [SessionController::class, 'updateBuildingSession'])->name('session.set.building');

    // Buildings
    Route::get('/buildings', [AdminBuildingController::class, 'index'])->name('admin.building.index');
    Route::get('/buildings/datatable', [AdminBuildingController::class, 'buildingList'])->name('admin.building.datatable');
    Route::get('/buildings/{buildingId}', [AdminBuildingController::class, 'show'])->name('admin.building.show');
    Route::post('/buildings', [AdminBuildingController::class, 'create'])->name('admin.building.create');
    Route::put('/buildings/{buildingId}', [AdminBuildingController::class, 'update'])->name('admin.building.update');
    Route::delete('/buildings/{buildingId}', [AdminBuildingController::class, 'delete'])->name('admin.building.delete');

    // Flats
    Route::get('/flats', [AdminFlatController::class, 'index'])->name('admin.flat.index');
    Route::get('/flats/datatable', [AdminFlatController::class, 'flatList'])->name('admin.flat.datatable');
    Route::get('/flats/{flatId}', [AdminFlatController::class, 'show'])->name('admin.flat.show');
    Route::post('/flats', [AdminFlatController::class, 'create'])->name('admin.flat.create');
    Route::put('/flats/{flatId}', [AdminFlatController::class, 'update'])->name('admin.flat.update');
    Route::delete('/flats/{flatId}', [AdminFlatController::class, 'delete'])->name('admin.flat.delete');
    Route::get('/buildings/{buildingId}/flats', [AdminFlatController::class, 'getByBuilding'])->name('admin.flat.getByBuilding');

});
require __DIR__.'/auth.php';
