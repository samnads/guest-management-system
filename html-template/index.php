<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lobby Visitors</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#1570b8">
<link rel="icon" type="image/png" href="images/logo-icon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>

</head>

<body>
<div class="wrapper-main">


<section class="v-center home-page text-center">

    <div class="row main-content m-0">
	    <div class="col-sm-12 welcome-image"><img src="images/lobby.png" alt="" /></div>
    	<div class="home-title Metropolis-Bold"><span class="Metropolis-Regular">Welcome to</span><br />Regency Pearl 6</div>
		<div class="n-btn-main"><a href="purpose-of-visit.php" class="n-btn">Sign In</a></div>
	</div><!--main-content end-->
	
	<footer class="az-logo"><img src="images/azinova-logo.png" alt="Azinova" /></footer>
</section><!--HOME Section End-->

	
</div>
</body>
</html>
