<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lobby Visitors</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#1570b8">
<link rel="icon" type="image/png" href="images/logo-icon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>

</head>

<body>
<div class="wrapper-main">

<section class="v-center purpose-visit-page text-center">
    <header>
	    <div class="row m-0">
		     <div class="col-6 back-btn-main p-0">
			      <a href="index.php"><div class="back-btn pull-left goto-home-btn">Back</div></a>
			 </div>
			 <div class="col-6 p-0 text-white text-right">Time</div>
		</div>
	</header>
	
	
	<div class="row main-content m-0">
	
		<div class="home-title Metropolis-Bold"><span class="Metropolis-Regular">Purpose of</span> VISIT</div>
		<div class="n-btn-main">
			<a href="guest.php" class="n-btn">Guest</a>
			<a href="#" class="n-btn">Delivery</a>
			<a href="#" class="n-btn">Cleaning</a>
			<a href="#" class="n-btn">Maintenance</a>
			<a href="#" class="n-btn">Other</a>
		</div>
	</div><!--main-content end-->
	
	<div class="col-12 qrcode-main">
	    <div class="qrcode"><img src="images/qrcode.png" alt="" /></div>
	</div>
	
	<footer class="az-logo"><img src="images/azinova-logo.png" alt="Azinova" /></footer>
</section><!--Purpose of Visit Section End-->

	
</div>
</body>
</html>
