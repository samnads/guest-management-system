<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lobby Visitors</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#1570b8">
<link rel="icon" type="image/png" href="images/logo-icon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>

<link rel="stylesheet" type="text/css" href="css/datepicker.css"/>

</head>

<body>
<div class="wrapper-main">


<section class="v-center guest-page">
    <header>
	    <div class="row m-0">
		     <div class="col-6 back-btn-main p-0">
			      <a href="purpose-of-visit.php"><div class="back-btn pull-left goto-purpose-visit-btn">Back</div></a>
			 </div>
			 <div class="col-6 p-0 text-white text-right">Time</div>
		</div>
	</header>
	
	<form id="service-booking-form">
	<div class="row main-content m-0">
	     <div class="col-lg-8 col-md-12 m-cont-left p-0">
		      
		      <div class="row m-0">
			       <div class="col-md-6 col-sm-6 cm-field-main">
				        <div class="field-icon v-center"><img src="images/apartment.png" alt="" /></div>
						<p>Select Apartment Number</p>
						<select name="apartment_number" class="w90" name="apartment_name" required="required" autocomplete="off">
						     <option value="0">Select</option>
							 <option value="1">Number 01</option>
							 <option value="2">Number 02</option>
							 <option value="3">Number 03</option>
							 <option value="4">Number 04</option>
							 <option value="5">Number 05</option>
						</select>
				   </div>
				   
				   
				   <div class="col-md-6 col-sm-6 cm-field-main">
				        <div class="field-icon v-center"><img src="images/smartphone.png" alt="" /></div>
						<p>Mobile Number</p>
						<input class="input-field w90" placeholder="" name="mobile-number" type="number" required="required" autocomplete="off">
				   </div>
				   
				   
				   <div class="col-md-6 col-sm-6 cm-field-main">
				        <div class="field-icon v-center"><img src="images/user.png" alt="" /></div>
						<p>Name *</p>
						<input class="input-field w90" placeholder="" name="full_name" type="text" required="required" autocomplete="off">
				   </div>
				   
				   
				   <div class="col-md-6 col-sm-6 cm-field-main">
				        <div class="field-icon v-center"><img src="images/office-building.png" alt="" /></div>
						<p>Company Name *</p>
						<input class="input-field w90" placeholder="" name="company_name" type="text" required="required" autocomplete="off">
				   </div>
				   
				   
				   <div class="col-md-6 col-sm-6 cm-field-main">
				        <div class="field-icon v-center"><img src="images/license.png" alt="" /></div>
						<p>ID Number *</p>
						<input class="input-field w90 id-icon" placeholder="" name="id_number" type="number" required="required" autocomplete="off">
				   </div>
				   
				   
				   <div class="col-md-6 col-sm-6 cm-field-main">
						<div class="field-icon v-center"><img src="images/calendar.png" alt="" /></div>
						<p>ID Expiry Date *</p>
						<input id="datepicker" class="input-field w90" name="datepicker" type="text" required="required" autocomplete="off" data-provide="datepicker">
				   </div>
			  </div>
			  
		 </div>
		 
		 <div class="col-lg-4 col-md-12 m-cont-right p-0">
		      <div class="col-sm-12 user-photo"><img src="images/user-photo.jpg" alt="" /></div>
			  
			  <div class="col-sm-12 user-photo-btn">
			       <label class="cameraButton">Take a picture
                        <input type="file" accept="image/*;capture=camera">
                   </label>
			  </div>
		   
		 </div>
		 
		 <div class="col-md-12 m-cont-bottom cm-field-main text-center p-0">
			  <input type="submit" class="n-btn" value="Submit Your Details" id="">
		 </div>
	</div><!--main-content end-->
	</form>
	
	<div class="col-12 qrcode-main">
	    <div class="qrcode"><img src="images/qrcode.png" alt="" /></div>
	</div>
	
	
	<footer class="az-logo"><img src="images/azinova-logo.png" alt="Azinova" /></footer>
</section><!--Guest Section End-->



<div class="common-popup-wrapper">
	<div class="common-popup-section d-flex">
		<div class="common-popup-main p-0 col-md-6 col-sm-6">
			<div class="close-btn" onclick="closeFancy()"><img src="https://booking.emaid.info:3443/elite-demo/images/close-big-w.webp" alt=""></div>
			<div class="common-popup-content-main text-center m-0">
			     <div class="success-icon"><img src="images/az.gif" alt="" /></div>
                 <h4>Your requested submitted <span class="Metropolis-Bold text-capital">successfully!</span></h4>
			</div>
		</div>
	</div>
</div>


  
<script type="text/javascript" src="js/jquery-3.6.0.min.js"></script> 
<script type="text/javascript" src="js/datepicker.js"></script>

<script type="text/javascript">
$(document).ready(function() {

	$(".close-btn").click(function(){
        $(".common-popup-wrapper").hide(500);
    });
	
	$('.datepicker').datepicker();
	
});
</script>


 

  
  
  
</div>
</body>
</html>
