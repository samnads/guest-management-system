<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuildingsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('buildings')) {
            Schema::create('buildings', function (Blueprint $table) {
                $table->bigIncrements('building_id')->unsigned();
                $table->string('building_name', 500)->nullable()->default(null);
                $table->string('code', 100)->nullable()->default(null);
                $table->string('device_type', 10)->nullable()->default(null);
                $table->text('device_id')->nullable()->default(null);
                $table->tinyInteger('is_default')->default(0);
                $table->tinyInteger('status')->default(1);
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('buildings');
    }
}
