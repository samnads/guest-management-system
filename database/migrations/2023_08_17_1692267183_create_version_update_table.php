<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersionUpdateTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('version_update')) {
            Schema::create('version_update', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('device_type', 100)->nullable()->default(null);
                $table->double('new_version')->nullable()->default(null);
                $table->string('type', 100)->nullable()->default(null);
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('version_update');
    }
}
