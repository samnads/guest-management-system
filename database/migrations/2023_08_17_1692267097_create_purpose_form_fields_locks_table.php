<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurposeFormFieldsLocksTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('purpose_form_fields_locks')) {
            Schema::create('purpose_form_fields_locks', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->bigInteger('purpose_form_field_id')->unsigned();
                $table->foreign('purpose_form_field_id')->references('purpose_form_field_id')->on('purpose_form_fields');
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('purpose_form_fields_locks');
    }
}
