<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuestUserTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('guest_user')) {
            Schema::create('guest_user', function (Blueprint $table) {
                $table->bigIncrements('user_id')->unsigned();
                $table->string('name', 100)->nullable()->default(null);
                $table->string('mobile', 100)->nullable()->default(null);
                $table->string('qatarid', 100)->nullable()->default(null);
                $table->string('location', 500)->nullable()->default(null);
                $table->string('description', 1500)->nullable()->default(null);
                $table->string('purpose', 100)->nullable()->default(null);
                $table->unsignedBigInteger('p_v_id')->nullable()->default(null);
                $table->bigInteger('building_id')->unsigned()->nullable()->default(null);
                $table->bigInteger('flat_id')->unsigned()->nullable()->default(null);
                $table->string('image', 100)->nullable()->default(null);
                $table->dateTime('create_date')->nullable()->default(null);
                $table->tinyInteger('accepted_status')->default(0);
                $table->text('device_id')->nullable()->default(null);
                $table->string('device_type', 50)->nullable()->default(null);
                $table->tinyInteger('status')->default(1);
                $table->string('qrcodeDetail', 200)->nullable()->default(null);
                $table->string('idnumber', 200)->nullable()->default(null);
                $table->date('id_expiry_date')->nullable()->default(null);
                $table->string('company_name', 250)->nullable()->default(null);

                // Remove the foreign key constraints here, we'll add them later

                $table->timestamps();
            });

            // Define the foreign key constraints after creating the table
            Schema::table('guest_user', function (Blueprint $table) {
                $table->foreign('p_v_id')->references('p_v_id')->on('purpose_of_visit');
                $table->foreign('building_id')->references('building_id')->on('buildings');
                $table->foreign('flat_id')->references('flat_id')->on('flats');
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('guest_user');
    }
}
