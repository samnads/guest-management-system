<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlatsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('flats')) {
            Schema::create('flats', function (Blueprint $table) {
                $table->bigIncrements('flat_id')->unsigned();
                $table->string('flat_num', 500)->nullable()->default(null);
                $table->bigInteger('building_id')->unsigned();
                $table->string('username', 100)->nullable()->default(null);
                $table->string('password', 100)->nullable()->default(null);
                $table->string('device_type', 150)->nullable()->default(null);
                $table->text('device_id')->nullable()->default(null);
                $table->tinyInteger('status')->default(1);
                $table->foreign('building_id')->references('building_id')->on('buildings');
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('flats');
    }
}
