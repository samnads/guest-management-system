<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcessCodesTable extends Migration
{
    public function up()
    {
        Schema::create('acess_codes', function (Blueprint $table) {
            if (!Schema::hasTable('access_codes')) {
                $table->bigIncrements('code_id');
                $table->string('code_text', 250)->nullable()->default(null);
                $table->integer('code_building')->nullable()->default(null);
                $table->string('code_token', 250)->nullable()->default(null);
                $table->string('code_device_type', 100)->nullable()->default(null);
                $table->tinyInteger('code_status', false, true)->nullable()->default(null); // Note: Set auto-increment to false
                $table->dateTime('code_added_datetime')->nullable()->default(null);
            }
        });
    }

    public function down()
    {
        Schema::dropIfExists('acess_codes');
    }
}
