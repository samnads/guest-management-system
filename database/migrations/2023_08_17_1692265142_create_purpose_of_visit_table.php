<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurposeOfVisitTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('purpose_of_visit')) {
            Schema::create('purpose_of_visit', function (Blueprint $table) {
                $table->bigIncrements('p_v_id');
                $table->string('purpose', 100)->nullable()->default(null);
                $table->string('sort_order', 55)->nullable()->default(null);
                $table->tinyInteger('status')->default(1);
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('purpose_of_visit');
    }
}
