<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurposeFormFieldsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('purpose_form_fields')) {
            Schema::create('purpose_form_fields', function (Blueprint $table) {
                $table->bigIncrements('purpose_form_field_id')->unsigned();
                $table->unsignedBigInteger('p_v_id');
                $table->string('table_name', 100);
                $table->string('table_column', 70);
                $table->enum('field_type', ['input', 'textarea', 'select']);
                $table->enum('select_options_variable', ['my_flats'])->nullable()->default(null);
                $table->string('select_options_default_label', 155)->nullable()->default(null);
                $table->enum('field_input_type', ['text', 'number', 'password', 'checkbox', 'radio', 'date'])->nullable()->default(null);
                $table->tinyInteger('custom_date_picker')->nullable()->default(null);
                $table->string('field_name', 70);
                $table->string('field_placeholder', 70)->nullable()->default(null);
                $table->string('field_id', 70)->nullable()->default(null);
                $table->string('field_label', 100);
                $table->enum('field_is_required', ['1'])->nullable()->default(null);
                $table->string('field_is_required_error_message')->nullable()->default(null);
                $table->string('field_icon_file')->nullable()->default(null);
                $table->string('field_icon_html')->nullable()->default(null);
                $table->string('field_css_classes')->nullable()->default(null);
                $table->string('field_inline_css')->nullable()->default(null);
                $table->integer('form_order')->nullable()->default(null);
                $table->timestamps();
                $table->softDeletes()->nullable();
                $table->foreign('p_v_id')->references('p_v_id')->on('purpose_of_visit');
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('purpose_form_fields');
    }
}
