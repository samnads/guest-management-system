<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminUserTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('admin_user')) {
            Schema::create('admin_user', function (Blueprint $table) {
                $table->bigIncrements('user_id');
                $table->string('username', 200)->nullable()->default(null);
                $table->string('password', 200)->nullable()->default(null);
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('admin_user');
    }
}
