<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FlatsTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [1, 'FLAT-001', 1, 'flat1', '12345', 'android', 'c8y4DtsrSkK1PJIsvfieyV:APA91bGBF_ebad9yfEiLZFC-UzFrZnS5qSMLKwNgYAFU9pOwXhCIjdF5s-WeqcqF8h6ndsCEKDJQTueQgensltvPiQL0xvLPVpIaXRBuWtbRf_qjhdUh6I4JNLdVeOTfGlt5LSRSq2EE', 1],
            [2, 'FLAT-002', 1, 'flat2', '12345', 'ios', 'ehV5Ud_7rkqgvm428aN9Rx:APA91bELs5FspsIA4QLN-QlCE7EQ_rdaJSdrCycEEaFtKRe-aq3cibbyhNNTtRWsDmartksu2ssFdcXymif10Lace0_qhviGYBh8ptw5huGIoB8KHUnLbCB4VCOId2w6djayD8BqQKx7', 1],
            [3, 'Office', 1, 'office', '12345', '', '', 1],
            [4, 'Security', 1, 'security', '12345', 'android', 'cXlFeDKmRxmVKs2LQnQYS2:APA91bHh_s8_8uuJ18XccM1gZX8Ia-0w36wnLax9ATZ6ibW_xXdTt7_xSbrYGMA0Rthy48XFdq4e1h9wsf2Is36AVGkACfHQDIqivyN2_35PijFBm-HF0DkSXXa_U8mXKbKKxMJ6U_OG', 1],
            [5, 'Apartment 01', 1, 'apt01', '20507', '', '', 1],
            [6, 'Apartment 02', 1, 'apt02', '20995', 'ios', '23db7005dfa8857b02881f7043eed6f1eeb1a14688ce5f350c7397314cc4700d', 1],
            [7, 'Apartment 03', 1, 'apt03', '30737', 'android', 'dBHKnOHXQde5h7aWkv12EE:APA91bHaQUEJ54pTQH-2WbbV8R_Rv8Dk-nDLFz08hc7taF4L2wlMU6PhByOVphusHsHUHiVuZe60LdvL8ntJ6ZoeXYFs1PN7iSwgnSiGpGEYFmwzm0S1w6REV_vRpht1wIgsp_wBhf2W', 1],
            [8, 'Apartment 04', 1, 'apt04', '26293', 'android', 'dkPEi5XhRdSbbGCT6i0hzu:APA91bFDKdmbX_z5jn4YyH3eecqTUnmIyVSnGWthqP1dtEqzNF8xc-peIZzjDMkPXFP7RT6Z_Mb7Mqs4hNezgK0IZ8Y-wOP0dv5jQZfS0fXWz4As0y2yVm2VDFHbVoVXog_rjhf_TutC', 1],
            [9, 'Apartment 05', 1, 'apt05', '60935', NULL, NULL, 1],
            [10, 'Apartment 06', 1, 'apt06', '74749', NULL, NULL, 1],
            [11, 'Apartment 07', 1, 'apt07', '23712', NULL, NULL, 1],
        ];

        // Insert data into the 'flats' table
        DB::table('flats')->insert($data);
    }
}
