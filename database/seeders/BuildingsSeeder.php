<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BuildingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [1, 'Regency Residence Al Sadd 12', 'RR001', NULL, NULL, 0, 0],
            [2, 'Building2', 'BD002', NULL, NULL, 0, 0],
            [3, 'Gulf Residence 17-A', 'GR17A', NULL, NULL, 0, 1],
            [4, 'Regency Residence Al Sadd 6', 'RRAS6', NULL, NULL, 0, 0],
            [5, 'Gulf Residence 13', 'GR13', NULL, NULL, 0, 0],
            [6, 'Gulf Residence 19', 'GR19', NULL, NULL, 0, 0],
            [7, 'Regency Residence Al Sadd 7', 'RRAS7', NULL, NULL, 0, 0],
            [8, 'Regency Residence Al sadd 10', 'RRAS10', NULL, NULL, 0, 0],
            [9, 'Regency Residence Al sadd 14', 'RRAS14', NULL, NULL, 0, 0],
            [10, 'Regency pearl 2 PA 20', 'RRAS13', NULL, NULL, 0, 1],
            [11, 'Gulf Residence 18-B', 'GR18B', NULL, NULL, 0, 1],
            [12, 'Gulf Residence 18-A', 'GR18A', NULL, NULL, 0, 1],
            [13, 'Gulf Residence 17-B', 'GR17B', NULL, NULL, 0, 1],
            [14, 'Gulf Residence 20', 'GR20', NULL, NULL, 0, 0],
            [15, 'Gulf Residence 9', 'GR9', NULL, NULL, 0, 0],
            [16, 'Gulf Residence 9', 'GR9', NULL, NULL, 0, 0],
            [17, 'Regency 13 Apartment', '13APT', NULL, NULL, 0, 1],
            [18, 'TEST', 'TEST', NULL, NULL, 0, 1],
            [19, 'Regency Residence Musheireb 1', 'RRMU1', NULL, NULL, 0, 0],
            [20, 'Regency Residence Musheireb 4', 'RRMU4', NULL, NULL, 0, 0],
            [21, 'Regency Residence Musheireb 6', 'RRUM6', NULL, NULL, 0, 0],
            [22, 'Regency Residence Musheireb 7', 'RRUM7', NULL, NULL, 0, 0],
            [23, 'RETAJ', 'RETAJ', NULL, NULL, 0, 0],
            [24, 'Gulf Residence 10', 'GR10', NULL, NULL, 0, 0],
            [25, 'PORTO 20', 'PR20A', NULL, NULL, 0, 1],
            [26, 'Paramount Residence PA12', 'PRPA12', NULL, NULL, 0, 1],
            [27, 'Regency Pearl 4 VB19', 'RPVB419', NULL, NULL, 0, 1],
            [28, 'Regency Pearl 5 VB6', 'RPVB56', NULL, NULL, 0, 1],
            [29, 'Regency Pearl 6 VB26', 'RPVB626', NULL, NULL, 1, 1],
            [30, 'Floresta Garden 103', 'FG103', NULL, NULL, 0, 1],
        ];

        foreach ($data as $row) {
            DB::table('buildings')->insert([
                'building_id' => $row[0],
                'building_name' => $row[1],
                'code' => $row[2],
                'device_type' => $row[3],
                'device_id' => $row[4],
                'is_default' => $row[5],
                'status' => $row[6],
            ]);
        }
    }
}
